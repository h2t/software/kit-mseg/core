The MSeg Motion Segmentation Evaluation Framework
===

**MSeg** is a framework for the [ArmarX](https://armarx.humanoids.kit.edu/index.html) robot development environment,
built to run and evaluate motion segmentation algorithms written in C++, Java, Python or MATLAB. Please refer to the
[MSeg documentation](https://mseg.readthedocs.io/) for guides on how to install the framework and how to get started.

If you encounter a problem, feel free to open a new issue in the
[issue tracker](https://gitlab.com/h2t/kit-mseg/mseg/issues) or send an email to
<a href="mailto:incoming+h2t/kit-mseg/mseg@gitlab.com">incoming+h2t/kit-mseg/mseg@gitlab.com</a>
(this will create an issue in the issue tracker).

--- *The rest of this document is relevant for developers only* ---


# MSeg Core Module

This is the repository of the MSeg Core Module, which consists of the GUI plugin for ArmarXGui, and three ArmarX
components. Those components are responsible for the communication between the Core Module and the PLIs
(`DataExchange`), and for the segmentation and evaluation (`SegmentationController`, resp. `EvaluationController`).

Contrary to other ArmarXGui-projects the Ice interfaces are separated and in their own repository `core-ice.git`.


## Development

It is strongly recommended to use the MSeg Umbrella Repository `mseg.git` in conjunction with this repository.

This repository includes a makefile which can be used to execute common actions. The make rules are:

- `make .depsdeb`: Internal target for the MSeg Umbrella Repository to collect the deb Dependencies
- `make .bashrc`: Internal target for the MSeg Umbrella Repository to collect entries which should be added to
  `~/.bashrc`
- `make clean`: Deletes all artefacts which were generated, compiled or contain distribution files, i.e. the `./build`
  folder and the `./deb-dist` folder
- `make install`: Compiles and installs the project
- `make package`: Creates a Debian package which can be distributed. This rule expects that the current Git reference
  is a tag in the format `v{MAJOR}.{MINOR}.{PATCH}`, where the version corresponds to the one set in
  `./etc/cmake/ArmarXPackageVersion.cmake`
- `make dummypackage`: Creates a Debian dummy-package which is an alias for the actual package

The make rules `install` and `package` make use of the `THREADS` environment variable, to parallelise the build
process. Example: `THREADS=4 make install` to use 4 threads


# License

Licensed under the [GPL 2.0 License](./LICENSE.md).
