# Allow number of jobs to be set
THREADS ?= 1

# Internal variables
deb_dist = ./deb-dist

# Lists deb dependencies
.depsdeb:
	@echo armarx-armarxcore
	@echo armarx-armarxgui
	@echo cmake
	@echo equivs
	@echo freeglut3-dev
	@echo libalglib-dev
	@echo libboost-all-dev
	@echo libeigen3-dev
	@echo libjsoncpp-dev
	@echo libqt4-dev
	@echo libsoqt4-dev
	@echo libtinyxml-dev
	@echo mmmcore
	@echo mmmtools
	@echo simox
	@echo zeroc-ice35

# .bashrc entries
.bashrc:
	@printf "# <CORE>\n"
	@printf "export MSEG_CORE_DIR=\"$(realpath $(dir $(lastword $(MAKEFILE_LIST))))\"\n"
	@printf "# Add bin folder to path\n"
	@printf "export PATH=\"\$${MSEG_CORE_DIR}/build/bin:\$${PATH}\"\n"
	@printf "# </CORE>\n\n"

# Delte all build and dist folders
clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ${deb_dist} 2> /dev/null || true

# Compile and install the core module
install:
	@mkdir ./build 2> /dev/null || true
	cmake -B./build -H.
	make --no-print-directory --directory=./build -j${THREADS}

# Create a deb package
package: install dummypackage
	@mkdir ${deb_dist} 2> /dev/null || true
	make --no-print-directory --directory=./build package
	@mv ./build/*.deb ${deb_dist}/

# Create the dummy package
dummypackage:
	@(cd ./build && equivs-build mseg.control)
