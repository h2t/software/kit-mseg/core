/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::EvaluationController
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MSeg_EvaluationController_H
#define _ARMARX_COMPONENT_MSeg_EvaluationController_H

#include <iomanip>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

#include <ArmarXCore/core/Component.h>

#include <MSeg/components/Utility/Utility.h>
#include <MSeg/components/EvaluationController/StandardClassification.h>
#include <MSeg/components/EvaluationController/MarginClassification.h>
#include <MSeg/components/EvaluationController/IntegratedKernelClassification.h>
#include <MSeg/components/EvaluationController/PenalisedErrorMetric.h>
#include <MSeg/interface/DataTypes.h>
#include <MSeg/interface/Topics.h>
#include <MSeg/interface/EvaluationControllerInterface.h>

namespace mseg
{
    class EvaluationControllerPropertyDefinitions;
    class EvaluationController;
}

/**
 * @class EvaluationControllerPropertyDefinitions
 * @brief
 */
class mseg::EvaluationControllerPropertyDefinitions:
    public armarx::ComponentPropertyDefinitions
{

public:

    EvaluationControllerPropertyDefinitions(std::string prefix):
        armarx::ComponentPropertyDefinitions(prefix)
    {
        //defineRequiredProperty<std::string>("PropertyName", "Description");
        //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
    }

};

/**
 * @defgroup Component-EvaluationController EvaluationController
 * @ingroup MSeg-Components
 * A description of the component EvaluationController.
 *
 * @class EvaluationController
 * @ingroup Component-EvaluationController
 * @brief Brief description of class EvaluationController.
 *
 * Detailed description of class EvaluationController.
 */
class mseg::EvaluationController :
    virtual public armarx::Component,
    virtual public mseg::EvaluationControllerInterface
{

private:

    static const std::string K_RESULTS;
    static const std::string K_RESULTS_OVERALL;
    static const std::string K_INDIVIDUAL_RESULTS;
    static const std::string K_IKM_CLASSIFICATION;
    static const std::string K_MARGIN_CLASSIFICATION;
    static const std::string K_STANDARD_CLASSIFICATION;
    static const std::string K_RAW_CLASSIFICATION;
    static const std::string K_ROUGH_GRANULARITY;
    static const std::string K_MEDIUM_GRANULARITY;
    static const std::string K_FINE_GRANULARITY;

    int currentStep = 0;
    int totalSteps = 0;
    int percentage = 0;
    const int stepPerSignificance = 22; // (3 Significances * 7 measures + 1 PSE)             = (3 * 7) + 1)  = 22
    const int stepsForTotalResult = 42; // (2 Classifications * 3 Significances * 7 Measures) = (2 * 3 * 7)   = 42

    mseg::EvaluationProgressTopicPrx evaluationProgressTopic;

public:

    static const std::string EVA_ROOT_NAME;

public: // armarx::ManagedIceObject interface

    virtual std::string getDefaultName() const
    {
        return "EvaluationController";
    }

protected: // armarx::ManagedIceObject interface

    virtual void onInitComponent();
    virtual void onConnectComponent();
    virtual void onDisconnectComponent();
    virtual void onExitComponent();
    virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

public: // EvaluationControllerInterface

    mseg::FormattedEvaluationResultPtr evaluateAll(const mseg::SegmentationResultDataset&, const Ice::Current& c = Ice::Current());

private:

    mseg::EvaluationResultPtr evaluateWithGenericClassification(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr evaluateAllForSignificance(std::vector<int>, std::vector<int>, int, std::string);
    mseg::EvaluationResultPtr evaluateTotalResult(std::vector<mseg::EvaluationResultPtr> individualResults);

    mseg::EvaluationResultPtr evaluateWithStandardClassification(std::vector<int>, std::vector<int>, int);
    mseg::EvaluationResultPtr evaluateWithMarginClassification(std::vector<int>, std::vector<int>, int);
    mseg::EvaluationResultPtr evaluateWithIntegratedKernelClassification(std::vector<int>, std::vector<int>, int);
    mseg::EvaluationResultPtr evaluateWithPenalisedErrorMetric(std::vector<int>, std::vector<int>);

    mseg::EvaluationResultPtr precision(double, double);
    mseg::EvaluationResultPtr precision(int, int);
    mseg::EvaluationResultPtr precision(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr recall(double, double);
    mseg::EvaluationResultPtr recall(int, int);
    mseg::EvaluationResultPtr recall(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr accuracy(double, double, double, double);
    mseg::EvaluationResultPtr accuracy(int, int, int, int);
    mseg::EvaluationResultPtr accuracy(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr f1Score(double, double);
    mseg::EvaluationResultPtr f1Score(double, double, double);
    mseg::EvaluationResultPtr f1Score(int, int, int);
    mseg::EvaluationResultPtr f1Score(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);
    mseg::EvaluationResultPtr f1Score(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr classificationF1Score(double, double, double, double);
    mseg::EvaluationResultPtr classificationF1Score(int, int, int, int);
    mseg::EvaluationResultPtr classificationF1Score(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr mccScore(double, double, double, double);
    mseg::EvaluationResultPtr mccScore(int, int, int, int);
    mseg::EvaluationResultPtr mccScore(mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr);

    mseg::EvaluationResultPtr accumulateOverSet(std::string granularity, std::string measure, std::string classification, std::vector<mseg::EvaluationResultPtr> set);

    mseg::FormattedEvaluationResultPtr formatResult(const mseg::SegmentationResultDataset&, mseg::EvaluationResultPtr);
    mseg::FormattedEvaluationResultPtr toFormattedEvaluationResult(mseg::EvaluationResultPtr);

    std::string vectorIntToString(std::vector<int>);

    void resetProgress(int segmentationResultCount);
    void reportProgress();

};

#endif
