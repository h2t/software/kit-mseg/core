/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::MarginClassification
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/EvaluationController/MarginClassification.h>

std::tuple<mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr>
mseg::MarginClassification::getClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    int margin = 20;

    int tp = 0;
    int tn = 0;
    int fp = 0;
    int fn = 0;

    // Count true positives with respect of margin
    {
        for (int groundTruthPoint : groundTruth)
        {
            bool pointFound = false;

            for (int i = 0; i < 1 + margin; i++)
            {
                // Is there a segmentation point for each possible ground truth point +/- margin
                if (std::find(test.begin(), test.end(), groundTruthPoint + i) != test.end()
                    || std::find(test.begin(), test.end(), groundTruthPoint - i) != test.end())
                {
                    if (!pointFound)
                    {
                        pointFound = true;
                    }
                    else
                    {
                        fp++;
                    }
                }
            }

            if (pointFound)
            {
                tp++;
            }
        }
    }

    // Count false positives with respect of margin
    {
        for (int testPoint : test)
        {
            bool pointFound = false;

            for (int i = 0; i < 1 + margin; i++)
            {
                // Is there no ground truth point for each possible segmentation point +/- margin
                if (std::find(groundTruth.begin(), groundTruth.end(), testPoint + i) != groundTruth.end()
                    || std::find(groundTruth.begin(), groundTruth.end(), testPoint - i) != groundTruth.end())
                {
                    pointFound = true;
                    break;
                }
            }

            if (!pointFound)
            {
                fp++;
            }
        }
    }

    // Count false negatives with respect of margin
    {
        for (int groundTruthPoint : groundTruth)
        {
            bool pointFound = false;

            for (int i = 0; i < 1 + margin; i++)
            {
                // Is there no segmentation point for each possible ground truth point +/- margin
                if (std::find(test.begin(), test.end(), groundTruthPoint + i) != test.end()
                    || std::find(test.begin(), test.end(), groundTruthPoint - i) != test.end())
                {
                    pointFound = true;
                    break;
                }
            }

            if (!pointFound)
            {
                fn++;
            }
        }
    }

    // Calculate tn
    tn = frameCount - tp - fp - fn;

    // Prepare output

    mseg::EvaluationResultPtr tpResult = new mseg::EvaluationResult();
    tpResult->name = "tp";
    tpResult->isGroup = false;
    tpResult->score = (double) tp;
    tpResult->isScoreInt = true;

    mseg::EvaluationResultPtr tnResult = new mseg::EvaluationResult();
    tnResult->name = "tn";
    tnResult->isGroup = false;
    tnResult->score = (double) tn;
    tnResult->isScoreInt = true;

    mseg::EvaluationResultPtr fpResult = new mseg::EvaluationResult();
    fpResult->name = "fp";
    fpResult->isGroup = false;
    fpResult->score = (double) fp;
    fpResult->isScoreInt = true;

    mseg::EvaluationResultPtr fnResult = new mseg::EvaluationResult();
    fnResult->name = "fn";
    fnResult->isGroup = false;
    fnResult->score = (double) fn;
    fnResult->isScoreInt = true;

    return std::make_tuple(tpResult, tnResult, fpResult, fnResult);
}
