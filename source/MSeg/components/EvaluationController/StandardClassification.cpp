/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::StandardClassification
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/EvaluationController/StandardClassification.h>

std::tuple<mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr, mseg::EvaluationResultPtr>
mseg::StandardClassification::getClassification(std::vector<int> groundTruth, std::vector<int> test, int frameCount)
{
    int tp = 0;
    int tn = 0;
    int fp = 0;
    int fn = 0;

    for (int i = 0; i < frameCount; i++)
    {
        // If frame i is in groundTruth AND in test => TP
        if (std::find(groundTruth.begin(), groundTruth.end(), i) != groundTruth.end()
            && std::find(test.begin(), test.end(), i) != test.end())
        {
            tp++;
        }
        // If frame i is NOT in groundTruth AND in test => FP
        else if (std::find(groundTruth.begin(), groundTruth.end(), i) == groundTruth.end()
                 && std::find(test.begin(), test.end(), i) != test.end())
        {
            fp++;
        }
        // If frame i is in groundTruth AND NOT in test => FN
        else if (std::find(groundTruth.begin(), groundTruth.end(), i) != groundTruth.end()
                 && std::find(test.begin(), test.end(), i) == test.end())
        {
            fn++;
        }
        // If frame i is NOT in groundTruth AND NOT in test => TN
        else
        {
            tn++;
        }
    }

    // Prepare output

    mseg::EvaluationResultPtr tpResult = new mseg::EvaluationResult();
    tpResult->name = "tp";
    tpResult->isGroup = false;
    tpResult->score = (double) tp;
    tpResult->isScoreInt = true;

    mseg::EvaluationResultPtr tnResult = new mseg::EvaluationResult();
    tnResult->name = "tn";
    tnResult->isGroup = false;
    tnResult->score = (double) tn;
    tnResult->isScoreInt = true;

    mseg::EvaluationResultPtr fpResult = new mseg::EvaluationResult();
    fpResult->name = "fp";
    fpResult->isGroup = false;
    fpResult->score = (double) fp;
    fpResult->isScoreInt = true;

    mseg::EvaluationResultPtr fnResult = new mseg::EvaluationResult();
    fnResult->name = "fn";
    fnResult->isGroup = false;
    fnResult->score = (double) fn;
    fnResult->isScoreInt = true;

    return std::make_tuple(tpResult, tnResult, fpResult, fnResult);
}
