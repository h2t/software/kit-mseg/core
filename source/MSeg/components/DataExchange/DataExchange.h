/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::DataExchange
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MSeg_DataExchange_H
#define _ARMARX_COMPONENT_MSeg_DataExchange_H

#include <string>
#include <iostream>
#include <fstream>

#include <ArmarXCore/core/Component.h>

#include <MMM/MMMCore.h>
#include <MMM/Model/Model.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>

#include <MSeg/components/Utility/Utility.h>
#include <MSeg/interface/exceptions.h>
#include <MSeg/interface/Topics.h>
#include <MSeg/interface/DataExchangeInterface.h>

namespace mseg
{
    /**
     * @class DataExchangePropertyDefinitions
     * @brief
     */
    class DataExchangePropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DataExchangePropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            this->defineOptionalProperty<std::string>("DataExchange", "DataExchange", "Data exchange");
        }
    };

    /**
     * @defgroup Component-DataExchange DataExchange
     * @ingroup MSeg-Components
     * A description of the component DataExchange.
     *
     * @class DataExchange
     * @ingroup Component-DataExchange
     * @brief Brief description of class DataExchange.
     *
     * Detailed description of class DataExchange.
     */
    class DataExchange :
        virtual public armarx::Component,
        virtual public mseg::DataExchangeInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "DataExchange";
        }

        void internal_reset(const Ice::Current& c = ::Ice::Current());
        void internal_setMotionRecording(const std::string& path, const Ice::Current& c = ::Ice::Current());
        void internal_setGroundTruth(const std::vector<int>& groundTruth, const Ice::Current& c = ::Ice::Current());
        std::vector<int> internal_getKeyFrames(const ::Ice::Current& = ::Ice::Current());
        bool internal_getOnlineFetched(const Ice::Current& = Ice::Current());

        int getFrameCount(const Ice::Current& c = ::Ice::Current());
        std::string getMMMFile(const Ice::Current& c = ::Ice::Current());
        std::string getViconFile(const Ice::Current& c = ::Ice::Current());
        std::vector<int> getGroundTruth(const Ice::Current& c = ::Ice::Current());
        std::vector<float> getJointAnglesForFrame(int n, const Ice::Current& c = ::Ice::Current());

        void reportKeyFrame(int frameNumber, const Ice::Current& c = ::Ice::Current());
        void reportKeyFrames(const std::vector<int>&, const Ice::Current& c = ::Ice::Current());

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitComponent();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectComponent();

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectComponent();

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitComponent();

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

        std::string loadFile(std::string path);

    private:

        std::string filename;
        MMM::LegacyMotionPtr mmmMotion;
        std::vector<int> groundTruth;
        bool trainingMode = false;
        bool onlineFetched = true;
        int lastFetchedFrame = -1;
        std::vector<int> segmentationPoints;
        mseg::SegmentationProgressIndividualTopicPrx segmentationProgressIndividualTopic;

    };
}

#endif
