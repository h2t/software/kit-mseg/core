/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::DataExchange
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/DataExchange/DataExchange.h>

void
mseg::DataExchange::onInitComponent()
{
    this->offeringTopic("SegmentationProgressIndividual");
}

void
mseg::DataExchange::onConnectComponent()
{
    this->segmentationProgressIndividualTopic = this->getTopic<mseg::SegmentationProgressIndividualTopicPrx>("SegmentationProgressIndividual");
}

void
mseg::DataExchange::onDisconnectComponent()
{

}

void
mseg::DataExchange::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr
mseg::DataExchange::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new DataExchangePropertyDefinitions(
            getConfigIdentifier()));
}

void
mseg::DataExchange::internal_reset(const Ice::Current&)
{
    this->groundTruth.clear();
    this->trainingMode = false;
    this->onlineFetched = true;
    this->lastFetchedFrame = -1;
    this->segmentationPoints.clear();
}

void
mseg::DataExchange::internal_setMotionRecording(const std::string& path, const Ice::Current&)
{
    MMM::LegacyMotionReaderXMLPtr r(new MMM::LegacyMotionReaderXML());

    this->filename = path;
    this->mmmMotion = r->createMotionFromString(this->loadFile(this->filename), "", mseg::Utility::paths::getMMMDataPath() + "/");
}

void
mseg::DataExchange::internal_setGroundTruth(const std::vector<int>& groundTruth, const Ice::Current&)
{
    this->groundTruth = groundTruth;
    this->trainingMode = true;
}

std::vector<int>
mseg::DataExchange::internal_getKeyFrames(const Ice::Current&)
{
    std::vector<int> segs = this->segmentationPoints;

    this->segmentationPoints.clear();

    return segs;
}

bool
mseg::DataExchange::internal_getOnlineFetched(const Ice::Current&)
{
    return this->onlineFetched;
}

std::vector<int>
mseg::DataExchange::getGroundTruth(const Ice::Current&)
{
    if (this->trainingMode)
    {
        return this->groundTruth;
    }
    else
    {
        throw mseg::DataHandlingException("DataExchange not in training mode, accessing the ground truth is illegal.");
    }
}

int
mseg::DataExchange::getFrameCount(const Ice::Current&)
{
    return this->mmmMotion->getNumFrames();
}

std::string
mseg::DataExchange::getMMMFile(const Ice::Current&)
{
    ARMARX_INFO << "Loading MMM file " << this->filename;

    this->onlineFetched = false;

    return this->loadFile(this->filename);
}

std::string
mseg::DataExchange::getViconFile(const Ice::Current&)
{
    ARMARX_INFO << "Vicon File requested";

    this->onlineFetched = false;

    return "File.c3d";
}

std::vector<float>
mseg::DataExchange::getJointAnglesForFrame(int n, const Ice::Current&)
{
    if (n < 0 || n >= this->getFrameCount())
    {
        throw mseg::DataHandlingException("Requested frame '" + std::to_string(n) + "' is not in the range "
                                          "[0-" + std::to_string(this->getFrameCount() - 1) + "]");
    }

    MMM::LegacyMotionPtr motion = this->mmmMotion; // ToDo: Assumes one motion per file

    MMM::MotionFramePtr motionFrame = motion->getMotionFrame(n);

    if (this->lastFetchedFrame < n)
    {
        // Throttle topic publishings to the necessary ones
        this->segmentationProgressIndividualTopic->reportSegmentationProgressIndividual(n);

        this->lastFetchedFrame = n;
    }
    else
    {
        // If frames are not fetched in increasing order, it is considered not online fetched as well
        this->onlineFetched = false;
    }

    std::vector<float> output(motionFrame->joint.data(), motionFrame->joint.data() + motionFrame->joint.size());

    return output;
}

void
mseg::DataExchange::reportKeyFrame(int frameNumber, const Ice::Current&)
{
    if (frameNumber < 0 || frameNumber >= this->getFrameCount())
    {
        throw mseg::DataHandlingException("Reported key frame '" + std::to_string(frameNumber) + "' is not in the range "
                                          "[0-" + std::to_string(this->getFrameCount() - 1) + "]");
    }

    this->segmentationPoints.push_back(frameNumber);
}

void
mseg::DataExchange::reportKeyFrames(const std::vector<int>& frames, const Ice::Current&)
{
    for (int segPoint : frames)
    {
        this->reportKeyFrame(segPoint);
    }
}

/*void
mseg::DataExchange::reportConfidences(const FrameConfidences& confidences, const Ice::Current&)
{

}*/

/*void
mseg::DataExchange::reportSeperationIntervals(const SeperationIntervals& intervals, const Ice::Current&)
{

}*/

std::string
mseg::DataExchange::loadFile(std::string path)
{
    std::string filecontent = "";

    std::string line;
    std::ifstream filestream(this->filename);

    if (filestream.is_open())
    {
        ARMARX_INFO << "Reading file: " << this->filename;

        while (std::getline(filestream, line))
        {
            filecontent += line + '\n';
        }

        filestream.close();
    }

    return filecontent;
}
