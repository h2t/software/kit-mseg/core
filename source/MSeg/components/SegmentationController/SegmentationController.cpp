/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::SegmentationController
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/components/SegmentationController/SegmentationController.h>

void
mseg::SegmentationController::onInitComponent()
{
    this->usingProxy("DataExchange");

    this->offeringTopic("SegmentationProgress");
    this->usingTopic("SegmentationProgressIndividual");
}

void
mseg::SegmentationController::onConnectComponent()
{
    this->data = this->getProxy<mseg::DataExchangeInterfacePrx>("DataExchange");

    this->segmentationProgressTopic = this->getTopic<mseg::SegmentationProgressTopicPrx>("SegmentationProgress");
}

void
mseg::SegmentationController::onDisconnectComponent()
{

}

void
mseg::SegmentationController::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr
mseg::SegmentationController::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new SegmentationControllerPropertyDefinitions(
            getConfigIdentifier()));
}

void
mseg::SegmentationController::setDataset(const mseg::MotionRecordingDataset& dataset, const Ice::Current&)
{
    this->dataset = dataset;

    for (mseg::MotionRecording& mr : this->dataset.recordings)
    {
        if (mr.frameCount < 0)
        {
            this->data->internal_reset();
            this->data->internal_setMotionRecording(mr.path);

            mr.frameCount = this->data->getFrameCount();
        }
    }
}

std::vector<std::string>
mseg::SegmentationController::querySegmentationAlgorithms(const Ice::Current&)
{
    IceGrid::QueryPrx query = this->getProxy<IceGrid::QueryPrx>("IceGrid/Query");

    ARMARX_INFO << "Querying for registered segmentation algorithms...";

    Ice::ObjectProxySeq objects = query->findAllObjectsByType(mseg::SegmentationAlgorithmInterface::ice_staticId());

    std::vector<std::string> algorithms {};

    for (Ice::ObjectPrx object : objects)
    {
        try
        {
            mseg::SegmentationAlgorithmInterfacePrx algorithm = mseg::SegmentationAlgorithmInterfacePrx::checkedCast(object);

            std::string algorithmName = algorithm->internal_getName();

            ARMARX_INFO << "Algorithm '" << algorithmName << "' found";

            algorithms.push_back(algorithmName);
        }
        catch (Ice::ConnectionRefusedException e)
        {
            ARMARX_WARNING << "IceGrid returned an algorithm which seems to be unavailable.";
        }
    }

    return algorithms;
}

std::vector<mseg::AlgorithmParameter>
mseg::SegmentationController::getAlgorithmParameters(const std::string& algorithmName, const Ice::Current&)
{
    mseg::SegmentationAlgorithmInterfacePrx algorithm = this->getProxy<mseg::SegmentationAlgorithmInterfacePrx>(algorithmName);
    std::string parametersJsonString;

    try
    {
        parametersJsonString = algorithm->internal_getParameters();
    }
    catch (const Ice::NotRegisteredException& e)
    {
        throw mseg::AlgorithmOfflineException();
    }

    Json::Reader jsonReader;
    Json::Value parametersJson;

    jsonReader.parse(parametersJsonString, parametersJson);

    std::vector<mseg::AlgorithmParameter> algorithmParameters;

    Json::Value parameters = parametersJson["parameters"];

    for (int i = 0; i < (int) parameters.size(); i++)
    {
        mseg::AlgorithmParameter algorithmParameter;

        mseg::VarTypes type;

        if (parameters[i]["type"].asString() == "bool")
        {
            type = mseg::VarTypes::BoolType;
        }
        else if (parameters[i]["type"].asString() == "int")
        {
            type = mseg::VarTypes::IntType;

            algorithmParameter.intmin = (parameters[i].isMember("intmin"))
                                        ? parameters[i]["intmin"].asInt() : INT_MIN;
            algorithmParameter.intmax = (parameters[i].isMember("intmax"))
                                        ? parameters[i]["intmax"].asInt() : INT_MAX;
        }
        else if (parameters[i]["type"].asString() == "float")
        {
            type = mseg::VarTypes::FloatType;

            algorithmParameter.floatmin = (parameters[i].isMember("floatmin"))
                                          ? parameters[i]["floatmin"].asFloat() : - FLT_MAX;
            algorithmParameter.floatmax = (parameters[i].isMember("floatmax"))
                                          ? parameters[i]["floatmax"].asFloat() : FLT_MAX;
            algorithmParameter.decimals = (parameters[i].isMember("decimals"))
                                          ? parameters[i]["decimals"].asInt() : 2;
        }
        else if (parameters[i]["type"].asString() == "string")
        {
            type = mseg::VarTypes::StringType;
        }
        else if (parameters[i]["type"].asString() == "json")
        {
            type = mseg::VarTypes::JsonType;
        }
        else
        {
            ARMARX_ERROR << "Unknown parameter type";
            throw "Unknown parameter type";
        }

        algorithmParameter.name = parameters[i]["name"].asString();
        algorithmParameter.description = parameters[i]["description"].asString();
        algorithmParameter.type = type;
        algorithmParameter.defaultValue = parameters[i]["defaultValue"].asString();

        algorithmParameters.push_back(algorithmParameter);
    }

    return algorithmParameters;
}

void
mseg::SegmentationController::setAlgorithmParameters(
    const std::string& algorithmName,
    const std::vector<mseg::AlgorithmParameter>& algorithmParameters,
    const Ice::Current&)
{
    this->algorithmParameters = algorithmParameters;

    mseg::SegmentationAlgorithmInterfacePrx algorithm = this->getProxy<mseg::SegmentationAlgorithmInterfacePrx>(algorithmName);
    std::string parametersJsonString;

    Json::Value parametersJson;
    parametersJson["parameters"] = Json::arrayValue;

    for (mseg::AlgorithmParameter algorithmParameter : algorithmParameters)
    {
        Json::Value parameterJson;

        parameterJson["name"] = algorithmParameter.name;
        parameterJson["value"] = algorithmParameter.value;

        parametersJson["parameters"].append(parameterJson);
    }

    Json::FastWriter writer;

    parametersJsonString = writer.write(parametersJson);

    try
    {
        algorithm->internal_setParameters(parametersJsonString);
    }
    catch (const Ice::NotRegisteredException& e)
    {
        throw mseg::AlgorithmOfflineException();
    }
}

mseg::SegmentationResultDataset
mseg::SegmentationController::segmentDataset(const std::string& algorithmName, const Ice::Current&)
{
    ARMARX_INFO << "Segmenting dataset with '" << algorithmName << "'";

    mseg::SegmentationResultDataset segmentationDataset;
    std::vector<mseg::SegmentationResult> segmentationData;

    mseg::SegmentationAlgorithmInterfacePrx algorithm = this->getProxy<mseg::SegmentationAlgorithmInterfacePrx>(algorithmName);
    bool algorithmRequiresTraining;
    bool algorithmFetchedOnline = true;

    try
    {
        algorithmRequiresTraining = algorithm->internal_requiresTraining();
    }
    catch (const Ice::NotRegisteredException& e)
    {
        throw mseg::AlgorithmOfflineException();
    }

    if (algorithmRequiresTraining && this->dataset.recordings.size() < this->kCrossValidate)
    {
        ARMARX_WARNING << "Dataset has not enough entries for " << this->kCrossValidate << "-fold cross-validation";
    }

    int k = std::min((uint) this->dataset.recordings.size(), this->kCrossValidate);

    // Reset and initialize segmentation progress
    {
        // Reset frame counts
        this->frameCountTotal = 0;
        this->frameCountTotalFinished = 0;
        this->segmentationProgressPercentage = 0;

        for (mseg::MotionRecording mr : this->dataset.recordings)
        {
            this->frameCountTotal += mr.frameCount;
        }

        if (algorithmRequiresTraining)
        {
            this->frameCountTotal *= k;
        }

        this->segmentationProgressTopic->reportSegmentationProgress(0);
    }

    // Split dataset into parts of roughly equal size.
    std::vector<std::vector<mseg::MotionRecording> > dataSetSplit = this->splitDataset(this->dataset.recordings, k);

    for (uint i = 0; i < dataSetSplit.size(); i++)
    {
        std::vector<std::vector<mseg::MotionRecording> > splitCopy(dataSetSplit);

        // Get current testing set
        std::vector<mseg::MotionRecording> testingSet = splitCopy[i];

        if (algorithmRequiresTraining)
        {
            // If training is required, concatenate all other parts and use them as the training set.
            splitCopy.erase(splitCopy.begin() + i);

            std::vector<mseg::MotionRecording> trainingSet;

            for (uint j = 0; j < splitCopy.size(); j++)
            {
                trainingSet.insert(trainingSet.end(), splitCopy[j].begin(), splitCopy[j].end());
            }

            mseg::Granularity granularity;

            // Reset algorithm
            try
            {
                algorithm->internal_resetTraining();
                granularity = algorithm->internal_getTrainingGranularity();
            }
            catch (const Ice::NotRegisteredException& e)
            {
                throw mseg::AlgorithmOfflineException();
            }

            // Train all recordings in training set
            for (mseg::MotionRecording d : trainingSet)
            {
                std::vector<int> groundTruth = mseg::Utility::filterGroundTruthBySignificance(mseg::Utility::loadGroundTruth(d.path + ".txt"), granularity);

                this->data->internal_reset();
                this->data->internal_setMotionRecording(d.path);
                this->data->internal_setGroundTruth(groundTruth);

                ARMARX_INFO << "Training algorithm with '" << d.filename << "' on granularity '" << granularity << "'";

                try
                {
                    algorithm->internal_train();
                }
                catch (const Ice::NotRegisteredException& e)
                {
                    throw mseg::AlgorithmOfflineException();
                }

                // Report segmentation progress
                {
                    this->frameCountTotalFinished += d.frameCount;

                    this->segmentationProgressPercentage = (int)(((float) this->frameCountTotalFinished / (float) this->frameCountTotal) * 100.0f);

                    this->segmentationProgressTopic->reportSegmentationProgress(this->segmentationProgressPercentage);
                }
            }
        }

        // Test all recordings in testing set
        for (mseg::MotionRecording d : testingSet)
        {
            // Reset data receiver and data provider
            this->data->internal_reset();
            this->data->internal_setMotionRecording(d.path);

            ARMARX_INFO << "Having algorithm segment '" << d.filename << "'";

            try
            {
                // Execute segmentation
                algorithm->internal_segment();
            }
            catch (const Ice::NotRegisteredException& e)
            {
                throw mseg::AlgorithmOfflineException();
            }
            catch (const mseg::DataHandlingException& e)
            {
                throw e;
            }
            catch (const mseg::AlgorithmException& e)
            {
                throw e;
            }
            catch (const std::exception& e)
            {
                throw mseg::AlgorithmException(e.what());
            }

            // Report segmentation progress
            {
                this->frameCountTotalFinished += d.frameCount;

                this->segmentationProgressPercentage = (int)(((float) this->frameCountTotalFinished / (float) this->frameCountTotal) * 100.0f);

                this->segmentationProgressTopic->reportSegmentationProgress(this->segmentationProgressPercentage);
            }

            // Push segmentation result to output vector
            {
                mseg::SegmentationResult segmentationDate;

                segmentationDate.recording.filename = d.filename;
                segmentationDate.recording.path = d.path;
                segmentationDate.recording.frameCount = d.frameCount;
                segmentationDate.keyFrames = this->data->internal_getKeyFrames();

                segmentationData.push_back(segmentationDate);
            }

            // If all frames were fetched in increasing order, it is considered online fetched
            algorithmFetchedOnline = algorithmFetchedOnline && this->data->internal_getOnlineFetched();
        }
    }

    ARMARX_INFO << "Segmentation done. Preparing segmentation result";

    segmentationDataset.results = segmentationData;
    segmentationDataset.datasetName = this->dataset.datasetName;
    segmentationDataset.algorithmName = algorithmName;
    segmentationDataset.parameters = this->algorithmParameters;
    segmentationDataset.trainingRequired = algorithmRequiresTraining;
    segmentationDataset.onlineFetched = algorithmFetchedOnline;

    return segmentationDataset;
}

void
mseg::SegmentationController::reportSegmentationProgressIndividual(int currentFrame, const Ice::Current&)
{
    int newSegmentationProgressPercentage = (int)(((float)(this->frameCountTotalFinished + currentFrame) / (float) this->frameCountTotal) * 100.0f);

    if (this->segmentationProgressPercentage < newSegmentationProgressPercentage)
    {
        this->segmentationProgressPercentage = newSegmentationProgressPercentage;

        this->segmentationProgressTopic->reportSegmentationProgress(this->segmentationProgressPercentage);
    }
}

std::vector<std::vector<mseg::MotionRecording> >
mseg::SegmentationController::splitDataset(std::vector<mseg::MotionRecording> data, int k)
{
    std::vector<std::vector<mseg::MotionRecording> > split;

    for (int i = 0; i < k; i++)
    {
        int start = i * data.size() / k;
        int end = (i + 1) * data.size() / k;
        split.push_back(std::vector<mseg::MotionRecording>(data.begin() + start, data.begin() + end));
    }

    return split;
}
