/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::ArmarXObjects::Utility
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_MSeg_Utility_H
#define _ARMARX_COMPONENT_MSeg_Utility_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <tuple>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/logging/Logging.h>

namespace mseg
{
    namespace Utility
    {
        enum ReleaseType
        {
            SNAPSHOT,
            ALPHA,
            BETA,
            RELEASE_CANDIDATE,
            RELEASE
        };

        const ReleaseType MSEG_RELEASE_TYPE = mseg::Utility::ReleaseType::SNAPSHOT;

        const int MSEG_VERSION_MAJOR = 1;
        const int MSEG_VERSION_MINOR = 3;
        const int MSEG_VERSION_PATCH = 0;
        const int MSEG_VERSION_APPENDIX = 1;

        std::string getVersion();

        std::vector<std::tuple<int, int> > loadGroundTruth(std::string path);
        std::vector<int> filterGroundTruthBySignificance(std::vector<std::tuple<int, int> > groundTruth, int significance);
        std::string readFile(std::string path);
        void writeFile(std::string path, std::string file, std::string content);
        void deleteFile(std::string path, std::string file);
        std::vector<std::string> filesInFolder(std::string path);
        void mkdir(std::string path, std::vector<std::string> subfolders = std::vector<std::string>());
        std::string getEnv(std::string key);

        namespace paths
        {
            std::string getMSegHomePath(std::vector<std::string> subfolders = std::vector<std::string>());
            std::string getMotionDataPath(std::vector<std::string> subfolders = std::vector<std::string>());
            std::string getMMMDataPath(std::vector<std::string> subfolders = std::vector<std::string>());
            std::string getMSegConfigPath(std::vector<std::string> subfolders = std::vector<std::string>());
        }
    }
}

#endif
