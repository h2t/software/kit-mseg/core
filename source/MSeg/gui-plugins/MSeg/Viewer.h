/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::Viewer
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef VIEWER_H
#define VIEWER_H

#include <string>
#include <time.h>
#include <vector>
#include <iostream>
#include <ctime>
#include <sstream>
#include <fstream>

#include <boost/algorithm/string.hpp>

#include <ArmarXCore/core/logging/Logging.h>

#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoClipPlane.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoUnits.h>
#include <Inventor/nodes/SoBaseColor.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/sensors/SoSensor.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <Eigen/Geometry>

#include <VirtualRobot/VirtualRobotCommon.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationNode.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/Trajectory.h>

#include <MMM/MMMCore.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderC3D.h>
#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Model/Model.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/Model/ModelProcessor.h>
#include <MMMSimoxTools/MMMSimoxTools.h>

#include <QFileDialog>
#include <QLayout>
#include <QWidget>

namespace mseg
{
    class Viewer :
        public QWidget
    {
        Q_OBJECT

    public:

        Viewer(QWidget* parent);
        ~Viewer();

        void reset();

        void setupMMMView(std::map<std::string, VirtualRobot::RobotPtr>& robots, std::vector<MMM::LegacyMotionPtr>& motions);
        void showMMMFrame(std::map<std::string, VirtualRobot::RobotPtr>& robots, std::vector<MMM::LegacyMotionPtr>& motions, std::map<std::string, VirtualRobot::RobotNodeSetPtr>& currentNodeSets, int pos);
        void setupMarkerView(MMM::MarkerMotionPtr& markerMotion);
        void showMarkerFrame(MMM::MarkerMotionPtr& markerMotion, int frame);

    protected:

        std::map<std::string, VirtualRobot::RobotNodeSetPtr> robotNodeSets;

        MMM::ModelProcessorFactoryPtr modelFactory;

        // Robot Pointer and Seperator Nodes
        std::map<std::string, SoSeparator*> robotSeps;
        SoSeparator* sceneSep;
        SoSeparator* markerSep;
        SoSeparator* motionSep;
        SoSeparator* rootCoordSep;
        SoSeparator* floorSep;
        SoSwitch* swFloor;

        // UI Class and Viewer
        SoQtExaminerViewer* canvas;
        bool _bShowValues;
        bool timerSensorAdded;

    };
}

#endif // VIEWER_H
