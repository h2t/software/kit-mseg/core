/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXCore::gui-plugins::MotionSetPlayerWidget
 * \author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/MotionSetPlayerWidget.h>

mseg::MotionSetPlayerWidget::MotionSetPlayerWidget(QWidget* parent): QWidget(parent)
{
    this->widget.setupUi(this);

    this->connect(this->widget.buttonTogglePlay, SIGNAL(clicked()), this, SLOT(onButtonTogglePlayClicked()));
    this->connect(this->widget.buttonStop, SIGNAL(clicked()), this, SLOT(onButtonStopClicked()));
    this->connect(this->widget.buttonRewind, SIGNAL(clicked()), this, SLOT(onButtonRewindClicked()));
    this->connect(this->widget.buttonForward, SIGNAL(clicked()), this, SLOT(onButtonForwardClicked()));
    this->connect(this->widget.buttonPrevMotion, SIGNAL(clicked()), this, SLOT(onButtonPrevMotionClicked()));
    this->connect(this->widget.buttonNextMotion, SIGNAL(clicked()), this, SLOT(onButtonNextMotionClicked()));
    this->connect(this->widget.buttonOpenDataset, SIGNAL(clicked()), this, SLOT(onButtonOpenDatasetClicked()));

    this->connect(this->widget.comboDataset, SIGNAL(currentIndexChanged(int)), this, SLOT(onComboDatasetChanged(int)));
    this->connect(this->widget.sliderMotion, SIGNAL(valueChanged(int)), this, SLOT(onSliderMotionChanged(int)));

    // Disable motion slider and dataset combo
    this->widget.comboDataset->setEnabled(false);
    this->widget.sliderMotion->setEnabled(false);

    this->player = new mseg::MotionPlayer(this->widget.widgetMotionViewer);

    this->connect(this->player, SIGNAL(playProgress(int)), this, SLOT(onPlayProgress(int)));
    this->connect(this->player, SIGNAL(fileChanged()), this, SLOT(onFileChanged()));

    this->player->main();
}

mseg::MotionSetPlayerWidget::~MotionSetPlayerWidget()
{

}

void
mseg::MotionSetPlayerWidget::playNext()
{
    this->widget.comboDataset->setCurrentIndex(this->getCurrent() + 1);
}

void
mseg::MotionSetPlayerWidget::playPrevious()
{
    this->widget.comboDataset->setCurrentIndex(this->getCurrent() - 1);
}

void
mseg::MotionSetPlayerWidget::playFirst()
{
    this->widget.comboDataset->setCurrentIndex(0);
}

void
mseg::MotionSetPlayerWidget::playLast()
{
    this->widget.comboDataset->setCurrentIndex(this->dataset.recordings.size() - 1);
}

void
mseg::MotionSetPlayerWidget::setCurrent(int index)
{
    if (this->dataset.recordings.size() > 0 && 0 <= index && index < (int) this->dataset.recordings.size())
    {
        // Disable UI elements
        this->widget.buttonTogglePlay->setEnabled(false);
        this->widget.buttonRewind->setEnabled(false);
        this->widget.buttonStop->setEnabled(false);
        this->widget.buttonForward->setEnabled(false);
        this->widget.buttonOpenDataset->setEnabled(false);
        this->widget.sliderMotion->setEnabled(false);
        this->widget.comboDataset->setEnabled(false);
        this->widget.buttonPrevMotion->setEnabled(false);
        this->widget.buttonNextMotion->setEnabled(false);
        this->widget.groundTruthIndicator->setKeyFrames({});
        this->widget.groundTruthIndicator->repaint();
        this->widget.keyFrameIndicator->setKeyFrames({});
        this->widget.keyFrameIndicator->repaint();

        QString filePath = QString::fromStdString(this->dataset.recordings[index].path);

        if (filePath.endsWith(".xml"))
        {
            QtConcurrent::run(this->player, &mseg::MotionPlayer::loadMMM, filePath.toStdString());
        }

        if (filePath.endsWith(".c3d"))
        {
            QtConcurrent::run(this->player, &mseg::MotionPlayer::loadC3D, filePath.toStdString());
        }
    }
}

int
mseg::MotionSetPlayerWidget::getCurrent()
{
    return this->widget.comboDataset->currentIndex();
}

QString
mseg::MotionSetPlayerWidget::getCurrentFilename()
{
    return QString::fromStdString(this->dataset.recordings[this->getCurrent()].path);
}

bool
mseg::MotionSetPlayerWidget::isMotionSetOpen()
{
    return this->player->getCurrentMotion() != nullptr;
}

void
mseg::MotionSetPlayerWidget::onButtonOpenDatasetClicked()
{
    QString title("Open dataset or motion files");
    QString defaultFolder(QString::fromStdString(mseg::Utility::paths::getMotionDataPath()));
    QString mimeTypes("Dataset file (*.mds);; MMM and Vicon files (*.xml *.c3d)");

    QStringList pathStrings = QFileDialog::getOpenFileNames(this->widget.widgetMotionViewer, title, defaultFolder, mimeTypes);

    if (!pathStrings.empty())
    {
        emit datasetLoading();

        this->widget.buttonTogglePlay->setEnabled(false);
        this->widget.buttonRewind->setEnabled(false);
        this->widget.buttonStop->setEnabled(false);
        this->widget.buttonForward->setEnabled(false);
        this->widget.comboDataset->setEnabled(false);
        this->widget.sliderMotion->setEnabled(false);
        this->widget.buttonPrevMotion->setEnabled(false);
        this->widget.buttonNextMotion->setEnabled(false);

        this->clearSegmentationResults();

        this->dataset = {};

        for (int i = 0; i < pathStrings.size(); i++)
        {
            QString pathString = pathStrings[i];

            // Split current path for each folder
            QStringList pathList = pathString.split('/');

            // Extract filename and rest of the path
            QString filename = pathList.takeLast();
            QString path = pathList.join("/") + "/";

            if (pathString.endsWith(".mds"))
            {
                this->dataset.datasetName = filename.toStdString();

                // Read file line by line and push data to data vectors
                {
                    std::string line;
                    std::ifstream myfile((path + filename).toStdString());

                    if (myfile.is_open())
                    {
                        while (std::getline(myfile, line))
                        {
                            QStringList lineSplit = QString::fromStdString(line).split(";");

                            QString motionFilename = lineSplit[0];
                            QString frameCount = lineSplit[1];

                            mseg::MotionRecording mr;

                            mr.filename = motionFilename.toStdString();
                            mr.path = (path + motionFilename).toStdString();
                            mr.frameCount = frameCount.toInt();

                            this->dataset.recordings.push_back(mr);
                        }

                        myfile.close();
                    }
                }
            }
            else
            {
                mseg::MotionRecording mr;

                mr.filename = filename.toStdString();
                mr.path = (path + filename).toStdString();

                this->dataset.recordings.push_back(mr);
            }
        }

        // Update combobox of dataset, implicitly loads the first motion file, because the currentIndex signal gets triggered
        this->updateComboDataset();

        // Queue first recording in dataset
        this->hasDatasetChanged = true;
    }
}

void
mseg::MotionSetPlayerWidget::onButtonTogglePlayClicked()
{
    this->playing = !this->playing;
    this->widget.buttonTogglePlay->setIcon(QIcon(!this->playing ? ":/images/play.png" : ":/images/pause.png"));
    this->player->togglePlay();
}

void
mseg::MotionSetPlayerWidget::onButtonStopClicked()
{
    this->playing = false;
    this->widget.buttonTogglePlay->setIcon(QIcon(":/images/play.png"));
    this->player->pause();
    this->player->jumpToFrame(0);
}

void
mseg::MotionSetPlayerWidget::onButtonRewindClicked()
{
    this->player->jumpToFrame(0);
}

void
mseg::MotionSetPlayerWidget::onButtonForwardClicked()
{
    this->widget.buttonTogglePlay->setIcon(QIcon(!this->playing ? ":/images/play.png" : ":/images/pause.png"));
}

void
mseg::MotionSetPlayerWidget::onButtonPrevMotionClicked()
{
    this->playPrevious();
}

void
mseg::MotionSetPlayerWidget::onButtonNextMotionClicked()
{
    this->playNext();
}

void
mseg::MotionSetPlayerWidget::onComboDatasetChanged(int index)
{
    this->setCurrent(index);
}

void
mseg::MotionSetPlayerWidget::onSliderMotionChanged(int val)
{
    this->player->jumpToFrame(val);
}

void
mseg::MotionSetPlayerWidget::onSegmentationStarted()
{
    this->widget.buttonOpenDataset->setEnabled(false);
}

void
mseg::MotionSetPlayerWidget::onSegmentationFinished(bool success, bool autoEvaluate)
{
    if (!success || !autoEvaluate)
    {
        this->widget.buttonOpenDataset->setEnabled(true);
    }
}

void
mseg::MotionSetPlayerWidget::onEvaluationStarted()
{
    this->widget.buttonOpenDataset->setEnabled(false);
}

void
mseg::MotionSetPlayerWidget::onEvaluationFinished(bool success)
{
    this->widget.buttonOpenDataset->setEnabled(true);
}

void
mseg::MotionSetPlayerWidget::onPlayProgress(int frame)
{
    this->widget.sliderMotion->setValue(frame);
}

void
mseg::MotionSetPlayerWidget::onFileChanged()
{
    // Notify widgets about new frame count
    this->widget.sliderMotion->setMaximum(this->player->getCurrentMotion()->getNumFrames() - 1);
    this->widget.groundTruthIndicator->setMaxFrame(this->player->getCurrentMotion()->getNumFrames() - 1);
    this->widget.keyFrameIndicator->setMaxFrame(this->player->getCurrentMotion()->getNumFrames() - 1);

    // Load and display ground truth and segmentation Results
    {
        std::string groundTruthFileName = (this->getCurrentFilename() + ".txt").toStdString();
        std::vector<std::tuple<int, int> > groundTruth = mseg::Utility::loadGroundTruth(groundTruthFileName);

        this->widget.groundTruthIndicator->setKeyFrames(groundTruth);
        this->widget.groundTruthIndicator->repaint();
        this->updateSegmentationResult();
    }

    // Enable UI elements (if conditions met)
    this->widget.buttonTogglePlay->setEnabled(true);
    this->widget.buttonRewind->setEnabled(true);
    this->widget.buttonStop->setEnabled(true);
    this->widget.buttonForward->setEnabled(true);
    this->widget.buttonOpenDataset->setEnabled(true);
    this->widget.sliderMotion->setEnabled(true);
    this->widget.comboDataset->setEnabled(this->dataset.recordings.size() > 1);
    this->widget.buttonPrevMotion->setEnabled(this->getCurrent() > 0);
    this->widget.buttonNextMotion->setEnabled(this->getCurrent() + 1 < this->widget.comboDataset->count());

    // Emit signal to notify other components about new dataset
    if (this->hasDatasetChanged)
    {
        this->hasDatasetChanged = false;
        emit datasetChanged(this->dataset);
    }
}

void
mseg::MotionSetPlayerWidget::setSegmentationResults(std::vector<mseg::SegmentationResult> segmentationResults)
{
    this->segmentationResults = segmentationResults;

    this->updateSegmentationResult();
}

void
mseg::MotionSetPlayerWidget::updateSegmentationResult()
{
    if (this->getCurrent() < 0)
    {
        return;
    }

    this->widget.keyFrameIndicator->setKeyFrames({});

    for (mseg::SegmentationResult s : this->segmentationResults)
    {
        QString filename = QString::fromStdString(s.recording.path);

        if (filename == this->getCurrentFilename())
        {
            this->widget.keyFrameIndicator->setKeyFrames(s.keyFrames);
        }
    }

    this->widget.keyFrameIndicator->repaint();
}

void
mseg::MotionSetPlayerWidget::clearSegmentationResults()
{
    // Update widget
    this->setSegmentationResults({});
}

void
mseg::MotionSetPlayerWidget::updateComboDataset()
{
    this->widget.comboDataset->setCurrentIndex(-1);

    this->widget.comboDataset->clear();

    for (mseg::MotionRecording mr : this->dataset.recordings)
    {
        this->widget.comboDataset->addItem(QString::fromStdString(this->dataset.datasetName + " | " + mr.filename));
    }
}
