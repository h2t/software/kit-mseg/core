/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::EvaluationWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/EvaluationResultsWidget.h>

mseg::EvaluationResultsWidget::EvaluationResultsWidget(QWidget* parent)
    : QWidget(parent)
{
    QLayout* buttonLayout = new QHBoxLayout();

    // Buttons
    {
        this->buttonExportAsXml = new QPushButton("Export as XML");
        this->buttonExportAsJson = new QPushButton("Export as JSON");

        // Disable if there's nothing to export
        this->buttonExportAsXml->setEnabled(false);

        // Not yet implemented
        this->buttonExportAsJson->setEnabled(false);

        this->connect(this->buttonExportAsXml, SIGNAL(clicked()), this, SLOT(onButtonExportAsXmlClicked()));
        this->connect(this->buttonExportAsJson, SIGNAL(clicked()), this, SLOT(onButtonExportAsJsonClicked()));

        buttonLayout->addWidget(this->buttonExportAsXml);
        buttonLayout->addWidget(this->buttonExportAsJson);
    }

    // Tree view
    {
        // Setup tree view
        this->treeEvaluationResults = new QTreeView(this);

        // Setup model
        this->modelEvaluationResults = new QStandardItemModel();

        QStandardItem* name = new QStandardItem("Name");
        name->setEditable(false);

        QStandardItem* score = new QStandardItem("Value");
        score->setEditable(false);

        this->modelEvaluationResults->setHorizontalHeaderItem(0, name);
        this->modelEvaluationResults->setHorizontalHeaderItem(1, score);

        // Set model
        this->treeEvaluationResults->setModel(this->modelEvaluationResults);

        // Setup header
        this->treeEvaluationResults->header()->setMovable(false);
    }

    // Setup layout
    QGridLayout* layout = new QGridLayout();

    this->setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(buttonLayout, 0, 0);
    layout->addWidget(this->treeEvaluationResults);
}

mseg::EvaluationResultsWidget::~EvaluationResultsWidget()
{

}

void
mseg::EvaluationResultsWidget::clearEvaluationResults()
{
    this->modelEvaluationResults->removeRows(0, this->modelEvaluationResults->rowCount());
    this->buttonExportAsXml->setEnabled(false);
}

void
mseg::EvaluationResultsWidget::updateEvaluationResults(mseg::FormattedEvaluationResultPtr evaluationResults)
{
    this->clearEvaluationResults();

    this->evaluationResults = evaluationResults;

    QStandardItem* root = this->modelEvaluationResults->invisibleRootItem();
    root->setEditable(false);

    this->updateEvaluationResultsHierarchy(evaluationResults, root);

    this->treeEvaluationResults->resizeColumnToContents(0);

    // Enable export buttons
    this->buttonExportAsXml->setEnabled(true);
}

void
mseg::EvaluationResultsWidget::updateEvaluationResultsHierarchy(mseg::FormattedEvaluationResultPtr result, QStandardItem* root)
{
    if (!result->isGroup)
    {
        this->addNameValue(result->name, result->value, root);
    }
    else
    {
        std::vector<mseg::FormattedEvaluationResultPtr> results = result->subResults;

        if (result->collapseGroup)
        {
            for (mseg::FormattedEvaluationResultPtr r : results)
            {
                this->updateEvaluationResultsHierarchy(r, root);
            }
        }
        else
        {
            QStandardItem* category = new QStandardItem(QString::fromStdString(result->name));
            category->setEditable(false);

            root->appendRow(category);

            if (result->isGroupExpanded)
            {
                this->treeEvaluationResults->setExpanded(this->modelEvaluationResults->indexFromItem(category), true);
            }

            for (mseg::FormattedEvaluationResultPtr r : results)
            {
                this->updateEvaluationResultsHierarchy(r, category);
            }
        }
    }
}

void
mseg::EvaluationResultsWidget::onButtonExportAsXmlClicked()
{
    QString defaultName = "~/eva-result-" + QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss") + ".xml";

    QString fileName = QFileDialog::getSaveFileName(this, "Export evaluation result as XML", defaultName, "XML (*.xml)");

    this->exportAsXml(this->evaluationResults, fileName.toStdString());
}

void
mseg::EvaluationResultsWidget::onButtonExportAsJsonClicked()
{

}

void
mseg::EvaluationResultsWidget::addNameValue(std::string name, std::string value, QStandardItem* root)
{
    this->addNameValue(new QStandardItem(QString::fromStdString(name)), new QStandardItem(QString::fromStdString(value)), root);
}

void
mseg::EvaluationResultsWidget::addNameValue(QStandardItem* name, QStandardItem* value, QStandardItem* root)
{
    name->setEditable(false);
    value->setEditable(false);

    QList<QStandardItem*> row;
    row << name << value;

    root->appendRow(row);
}

void
mseg::EvaluationResultsWidget::exportAsXml(mseg::FormattedEvaluationResultPtr result, std::string path)
{
    TiXmlDocument doc;

    TiXmlDeclaration* decl = new TiXmlDeclaration("1.0", "", "");
    doc.LinkEndChild(decl);

    TiXmlElement* rootNode = this->evaluationResultToXmlElement(result);
    rootNode->SetAttribute("mseg-version", "1.1");

    doc.LinkEndChild(rootNode);

    doc.SaveFile(path);
}

TiXmlElement*
mseg::EvaluationResultsWidget::evaluationResultToXmlElement(mseg::FormattedEvaluationResultPtr result)
{
    std::string nodeName = result->groupName;

    if (nodeName == "")
    {
        nodeName = result->name;
        nodeName = boost::replace_if(nodeName, boost::is_any_of(" .,_/\\"), '-');
        boost::to_lower(nodeName);
    }

    TiXmlElement* node = new TiXmlElement(nodeName);

    if (result->groupAttributeName != "" && result->groupAttributeValue != "")
    {
        node->SetAttribute(result->groupAttributeName, result->groupAttributeValue);
    }

    if (result->isGroup)
    {
        for (mseg::FormattedEvaluationResultPtr subResult : result->subResults)
        {
            TiXmlElement* subNode = this->evaluationResultToXmlElement(subResult);

            node->LinkEndChild(subNode);
        }
    }
    else
    {
        if (result->type == mseg::VarTypes::BoolType)
        {
            std::string value = "";

            try
            {
                if (boost::to_lower_copy(result->value) == "yes" || boost::to_lower_copy(result->value) == "true" || std::stoi(result->value) != 0)
                {
                    value = "true";
                }
                else
                {
                    value = "false";
                }
            }
            catch (const std::invalid_argument&)
            {
                value = "false";
            }

            TiXmlText* text = new TiXmlText(value);

            node->LinkEndChild(text);
        }
        else if (result->type == mseg::VarTypes::JsonType && (result->groupName == "ground-truth" || result->groupName == "segmentation"))
        {
            Json::Reader jsonReader;
            Json::Value pointArray;

            jsonReader.parse(result->value, pointArray);

            TiXmlElement* points = new TiXmlElement("key-frames");

            for (int i = 0; i < (int) pointArray.size(); i++)
            {
                TiXmlElement* point = new TiXmlElement("key-frame");
                TiXmlText* pointText = new TiXmlText(std::to_string(pointArray[i].asInt()));

                point->LinkEndChild(pointText);
                points->LinkEndChild(point);
            }

            node->LinkEndChild(points);
        }
        else
        {
            TiXmlText* text = new TiXmlText(result->value);

            node->LinkEndChild(text);
        }
    }

    return node;
}
