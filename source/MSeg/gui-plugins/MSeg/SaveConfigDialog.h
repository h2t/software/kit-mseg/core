/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::SaveConfigDialog
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef SAVECONFIGDIALOG_H
#define SAVECONFIGDIALOG_H

#include <QPushButton>
#include <QDialog>
#include <QString>

#include <ArmarXCore/core/logging/Logging.h>

#include <ui_SaveConfigDialog.h>

class SaveConfigDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SaveConfigDialog(QWidget* parent);
    void setComboOptions(std::vector<std::string> options);
    std::string getSaveName();

public slots:
    void onComboSavedConfigsChanged(int index);
    void onTextNameChanged(QString text);

private:
    Ui::SaveConfigDialog saveDialog;
    std::vector<std::string> comboOptions;
};

#endif // SAVECONFIGDIALOG_H
