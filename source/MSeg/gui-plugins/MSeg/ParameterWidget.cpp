/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::ParameterWidget
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ParameterWidget.h"

mseg::ParameterWidget::ParameterWidget(QWidget* parent): QWidget(parent)
{
    this->widget.setupUi(this);

    this->reset();

    this->connect(this->widget.buttonSave, SIGNAL(clicked()), this, SLOT(onButtonSaveClicked()));
    this->connect(this->widget.buttonDelete, SIGNAL(clicked()), this, SLOT(onButtonDeleteClicked()));
    this->connect(this->widget.comboParameterConfigs, SIGNAL(currentIndexChanged(int)), this, SLOT(onComboParameterConfigsChanged(int)));
}

mseg::ParameterWidget::~ParameterWidget()
{

}

void
mseg::ParameterWidget::reset()
{
    this->widgetParameter = new QWidget(this);
    this->widget.wrapperParameter->addWidget(this->widgetParameter);
    QFormLayout* layout = new QFormLayout();
    this->widgetParameter->setLayout(layout);
}

void
mseg::ParameterWidget::clearParameterWidgets()
{
    this->widgetParameter->deleteLater();
    this->reset();
}

void
mseg::ParameterWidget::onComboParameterConfigsChanged(int index)
{
    // Enable/Disable buttons as necessary
    this->widget.buttonSave->setEnabled(true);
    this->widget.buttonDelete->setEnabled(true);

    if (index == this->widget.comboParameterConfigs->count() - 1)
    {
        this->widget.buttonDelete->setEnabled(false);
        return;
    }

    // Check wether there are unsaved modifications
    if (this->selectedConfigJson != this->configToJson(this->getParameterConfig()))
    {
        // Ask user what to do
        QMessageBox::StandardButton pressed = QMessageBox::question(
                this,
                "Unsaved changes",
                "You have unsaved changes in the current parameter configuration. Do you want to save these before opening another one?",
                QMessageBox::Yes | QMessageBox::No
                                              );

        // Open save dialog, iff the user wants to save.
        if (pressed == QMessageBox::Yes)
        {
            this->widget.comboParameterConfigs->setCurrentIndex(this->widget.comboParameterConfigs->count() - 1);
            this->onButtonSaveClicked();
        }
    }

    // Open chosen configuration

    // Special case: Default config chosen:
    if (index == 0)
    {
        this->setParameterConfig(this->selectedDefaults);
        this->widget.buttonDelete->setEnabled(false);
        return;
    }

    // Otherwise open config from file
    std::string configName = this->widget.comboParameterConfigs->currentText().toStdString();
    std::vector<mseg::AlgorithmParameter> parameterConfig = this->loadParameterConfig(configName);

    // Create UI
    this->setParameterConfig(parameterConfig);
}

void
mseg::ParameterWidget::onButtonSaveClicked()
{
    // Initialize save name with the currently selected config name
    std::string configName = this->widget.comboParameterConfigs->currentText().toStdString();

    // Special case: "Default configuration" or "New configuration" selected:
    int selectedIndex = this->widget.comboParameterConfigs->currentIndex();
    if (selectedIndex <= 0 || selectedIndex == this->widget.comboParameterConfigs->count() - 1)
    {
        // Ask user for the save name
        SaveConfigDialog dialog(this);
        dialog.setComboOptions(this->getSavedConfigs());

        if (dialog.exec() != QDialog::Accepted)
        {
            return;
        }

        configName = dialog.getSaveName();
    }

    // Jsonify currently set parameters and remember changes.
    std::string configJson = this->configToJson(this->getParameterConfig());
    this->selectedConfigJson = configJson;

    // Write to file
    mseg::Utility::writeFile(
        mseg::Utility::paths::getMSegConfigPath({"parameters", this->selectedAlgorithm}),
        configName + ".pconf",
        configJson
    );

    // Make sure the saved config gets selected in the combobox, if a new one was created.
    this->updateComboParameterConfigOptions();
    int index = this->widget.comboParameterConfigs->findText(QString::fromStdString(configName));
    if (index == -1)
    {
        this->displayException("File not saved", "Something went wrong while saving. Savefile could not be found.");
        return;
    }
    this->widget.comboParameterConfigs->setCurrentIndex(index);
}

void
mseg::ParameterWidget::onButtonDeleteClicked()
{
    mseg::Utility::deleteFile(
        mseg::Utility::paths::getMSegConfigPath({"parameters", this->selectedAlgorithm}),
        this->widget.comboParameterConfigs->currentText().toStdString() + ".pconf"
    );

    this->updateComboParameterConfigOptions();
}


std::vector<mseg::AlgorithmParameter>
mseg::ParameterWidget::getParameterConfig()
{
    std::vector<mseg::AlgorithmParameter> algorithmParameters;
    int items = (int)((QFormLayout*)(this->widgetParameter->layout()))->rowCount();

    for (int i = 0; i < items; i++)
    {
        QLayoutItem* item = ((QFormLayout*)(this->widgetParameter->layout()))->itemAt(i, QFormLayout::FieldRole);

        mseg::AlgorithmParameter algorithmParameter = ((mseg::AlgorithmParameterWidget*) item->widget())->getAlgorithmParameter();

        algorithmParameters.push_back(algorithmParameter);
    }

    return algorithmParameters;
}

void
mseg::ParameterWidget::setParameterConfig(std::vector<mseg::AlgorithmParameter> algorithmParameters)
{
    // Clear all previous parameter widgets
    this->clearParameterWidgets();

    // Iterate over each algorithm parameter and add corresponding widget to the UI
    for (mseg::AlgorithmParameter algorithmParameter : algorithmParameters)
    {
        QLabel* label = new QLabel(QString::fromStdString(algorithmParameter.name));
        label->setToolTip(QString::fromStdString(algorithmParameter.description));

        mseg::AlgorithmParameterWidget* widget = new mseg::AlgorithmParameterWidget(algorithmParameter);

        ((QFormLayout*)(this->widgetParameter->layout()))->addRow(label, widget);
    }

    // Stringify so that we can later test, wether the configuration was changed by the user.
    this->selectedConfigJson = this->configToJson(this->getParameterConfig());
}

void
mseg::ParameterWidget::setSelectedAlgorithm(std::string algorithmName, std::vector<mseg::AlgorithmParameter> defaultConfig)
{
    // Set new default config
    this->selectedAlgorithm = algorithmName;
    this->selectedDefaults = defaultConfig;

    // Update UI elements
    this->clearParameterWidgets();
    this->setParameterConfig(defaultConfig);
    this->widget.buttonSave->setEnabled(true);
    this->widget.buttonDelete->setEnabled(false);
    this->widget.comboParameterConfigs->setEnabled(true);
    this->updateComboParameterConfigOptions();
}

void
mseg::ParameterWidget::deselectAlgorithm()
{
    // Set empty default config
    this->selectedAlgorithm = "";
    this->selectedDefaults = {};

    // Update UI elements
    this->clearParameterWidgets();
    this->widget.buttonSave->setEnabled(false);
    this->widget.buttonDelete->setEnabled(false);
    this->widget.comboParameterConfigs->setEnabled(false);
    this->widget.comboParameterConfigs->setCurrentIndex(0);
}

std::vector<std::string>
mseg::ParameterWidget::getSavedConfigs()
{
    // Get a list of all files in the save folder corresponding to this algorithm.
    std::string configFolder = mseg::Utility::paths::getMSegConfigPath({"parameters", this->selectedAlgorithm});
    mseg::Utility::mkdir(configFolder);
    std::vector<std::string> files = mseg::Utility::filesInFolder(configFolder);

    // Extract proper save names
    std::vector<std::string> configs;
    for (std::string file : files)
    {
        boost::filesystem::path p(file);
        if (p.extension().string() == ".pconf")
        {
            configs.push_back(p.stem().string());
        }
    }

    // Sort alphabetically
    std::sort(configs.begin(), configs.end());

    return configs;
}

std::vector<mseg::AlgorithmParameter>
mseg::ParameterWidget::loadParameterConfig(std::string name)
{
    // Read file
    std::string configFolder = mseg::Utility::paths::getMSegConfigPath({"parameters", this->selectedAlgorithm});
    std::string fileContent = mseg::Utility::readFile(configFolder + this->widget.comboParameterConfigs->currentText().toStdString() + ".pconf");

    // This will later be returned and contain the parsed parameters
    std::vector<mseg::AlgorithmParameter> algorithmParameters;

    // Setup JSON API for parsing
    Json::Reader jsonReader;
    Json::Value parametersJson;
    jsonReader.parse(fileContent, parametersJson);
    Json::Value parameters = parametersJson["parameters"];

    // These vectors will hold information about things that went wrong during parsing:

    // Will contain parameters that are contained in the save file, but are not part of the currently set default config.
    std::vector<std::string> deletedParameters;
    // Will contain parameters that have mismatched types in the default config vs. in the save file.
    std::vector<std::string> mismatchedTypes;
    // Will keep track which parameters from the default config were matched by one of the parameters from the save file.
    std::vector<bool> matchedInDefaults(this->selectedDefaults.size(), false);

    // Iterate through all parameters in the save file
    for (uint i = 0; i < parameters.size(); i++)
    {
        std::string parameterName = parameters[i]["name"].asString();

        // Remember parameters that can not be matched with the default config.
        int index = this->findParameterIndexInDefaults(parameterName);
        if (index == -1)
        {
            deletedParameters.push_back(parameterName);
            continue;
        }

        mseg::AlgorithmParameter algorithmParameter;
        mseg::AlgorithmParameter defaultTemplate = this->selectedDefaults[index];

        // Remember parameters with mismatched types.
        if (defaultTemplate.type != parameters[i]["type"].asInt())
        {
            mismatchedTypes.push_back(parameterName);
            continue;
        }

        // If this is executed, there is a match for this parameter in the save file. Remember this.
        matchedInDefaults[index] = true;

        // Copy everything from the default config except for the value.
        algorithmParameter.name = defaultTemplate.name;
        algorithmParameter.description = defaultTemplate.description;
        algorithmParameter.type = defaultTemplate.type;
        algorithmParameter.intmin = defaultTemplate.intmin;
        algorithmParameter.intmax = defaultTemplate.intmax;
        algorithmParameter.floatmin = defaultTemplate.floatmin;
        algorithmParameter.floatmax = defaultTemplate.floatmax;
        algorithmParameter.decimals = defaultTemplate.decimals;
        algorithmParameter.value = parameters[i]["value"].asString();

        algorithmParameters.push_back(algorithmParameter);
    }

    // Check for every missed parameter in the default config and add them to the list if necessary.
    std::vector<std::string> missedParameters;
    for (uint i = 0; i < matchedInDefaults.size(); i++)
    {
        if (!matchedInDefaults[i])
        {
            mseg::AlgorithmParameter defaultTemplate = this->selectedDefaults[i];
            if (std::find(mismatchedTypes.begin(), mismatchedTypes.end(), defaultTemplate.name) == mismatchedTypes.end())
            {
                missedParameters.push_back(defaultTemplate.name);
            }
            algorithmParameters.push_back(defaultTemplate);
        }
    }

    // Check for all recognized errors and give a notification, if any occured.
    if (deletedParameters.size() > 0 || missedParameters.size() > 0 || mismatchedTypes.size() > 0)
    {
        std::string deleted = "\nNo longer existing: " + boost::algorithm::join(deletedParameters, ", ") + ".";
        std::string missed = "\nNo value found: " + boost::algorithm::join(missedParameters, ", ") + ".";
        std::string mismatched = "\nMismatched types: " + boost::algorithm::join(mismatchedTypes, ", ") + ".";

        std::string exception = "The parameter configuration you are loading is incompatible with the one the algorithm requires.";
        exception += deletedParameters.size() > 0 ? deleted : "";
        exception += missedParameters.size() > 0 ? missed : "";
        exception += mismatchedTypes.size() > 0 ? mismatched : "";

        this->displayException("Mismatching parameter sets", exception);
    }

    return algorithmParameters;
}

void
mseg::ParameterWidget::updateComboParameterConfigOptions()
{
    this->widget.comboParameterConfigs->clear();
    this->widget.comboParameterConfigs->addItem("Default Configuration", "default");

    std::vector<std::string> configs = this->getSavedConfigs();
    for (std::string config : configs)
    {
        this->widget.comboParameterConfigs->addItem(QString::fromStdString(config));
    }

    this->widget.comboParameterConfigs->addItem("New Configuration", "new");
}

void
mseg::ParameterWidget::displayException(std::string title, std::string message)
{
    QMessageBox error(this);
    error.setFixedSize(500, 200);
    error.warning(0, QString::fromStdString(title), QString::fromStdString(message));
}

int
mseg::ParameterWidget::findParameterIndexInDefaults(std::string parameterName)
{
    for (uint i = 0; i < this->selectedDefaults.size(); i++)
    {
        if (this->selectedDefaults[i].name == parameterName)
        {
            return i;
        }
    }

    return -1;
}

std::string
mseg::ParameterWidget::configToJson(std::vector<mseg::AlgorithmParameter> config)
{
    Json::Value parametersJson;
    parametersJson["parameters"] = Json::arrayValue;

    for (mseg::AlgorithmParameter algorithmParameter : config)
    {
        Json::Value parameterJson;

        parameterJson["name"] = algorithmParameter.name;
        parameterJson["value"] = algorithmParameter.value;
        parameterJson["type"] = algorithmParameter.type;

        parametersJson["parameters"].append(parameterJson);
    }

    Json::StyledWriter writer;
    return writer.write(parametersJson);
}
