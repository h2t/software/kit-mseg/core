/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    ArmarXCore::gui-plugins::MSegWidgetController
 * \author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/MSegWidgetController.h>

mseg::MSegWidgetController::MSegWidgetController()
{
    this->widget.setupUi(this->getWidget());

    this->initComboAlgorithms();
    this->initPollTimer();

    // Show parameters tab
    this->widget.widgetTabs->setCurrentWidget(this->widget.tabParameters);

    this->initConnections();
}

mseg::MSegWidgetController::~MSegWidgetController()
{
    //delete this->player;
    //delete this->dataset;
}

void
mseg::MSegWidgetController::loadSettings(QSettings* settings)
{

}

void
mseg::MSegWidgetController::saveSettings(QSettings* settings)
{

}

void
mseg::MSegWidgetController::onInitComponent()
{
    this->usingProxy("SegmentationController");
    this->usingProxy("EvaluationController");

    this->usingTopic("SegmentationProgress");
    this->usingTopic("EvaluationProgress");
}

void
mseg::MSegWidgetController::onConnectComponent()
{
    this->segmentationController = this->getProxy<mseg::SegmentationControllerInterfacePrx>("SegmentationController");
    this->evaluationController = this->getProxy<mseg::EvaluationControllerInterfacePrx>("EvaluationController");

    // Manually trigger refresh
    this->onButtonRefreshAlgorithmsClicked();
}

void
mseg::MSegWidgetController::initConnections()
{
    // Connect this
    this->connect(this, SIGNAL(algorithmSelected(std::string)), this, SLOT(onAlgorithmSelected(std::string)));

    // Connect buttons
    this->connect(this->widget.buttonRefreshAlgorithms, SIGNAL(clicked()), this, SLOT(onButtonRefreshAlgorithmsClicked()));
    this->connect(this->widget.buttonSegmentCurrent, SIGNAL(clicked()), this, SLOT(onButtonSegmentCurrentClicked()));
    this->connect(this->widget.buttonSegmentAll, SIGNAL(clicked()), this, SLOT(onButtonSegmentAllClicked()));
    this->connect(this->widget.buttonEvaluate, SIGNAL(clicked()), this, SLOT(onButtonEvaluateClicked()));

    // Connect motion set player widget
    this->connect(this->widget.widgetMotionSetPlayer, SIGNAL(datasetLoading()), this, SLOT(onDatasetLoading()));
    this->connect(this->widget.widgetMotionSetPlayer, SIGNAL(datasetChanged(mseg::MotionRecordingDataset)), this, SLOT(onDatasetChanged(mseg::MotionRecordingDataset)));

    this->connect(this, SIGNAL(segmentationStarted()), this->widget.widgetMotionSetPlayer, SLOT(onSegmentationStarted()));
    this->connect(this, SIGNAL(segmentationFinished(bool, bool)), this->widget.widgetMotionSetPlayer, SLOT(onSegmentationFinished(bool, bool)));
    this->connect(this, SIGNAL(evaluationStarted()), this->widget.widgetMotionSetPlayer, SLOT(onEvaluationStarted()));
    this->connect(this, SIGNAL(evaluationFinished(bool)), this->widget.widgetMotionSetPlayer, SLOT(onEvaluationFinished(bool)));

    // Connect Comboboxes and Sliders
    this->connect(this->widget.comboAlgorithm, SIGNAL(currentIndexChanged(int)), this, SLOT(onComboAlgorithmChanged()));

    // Connect signals from other threads
    this->connect(this, SIGNAL(segmentationProgressChanged(int)), this, SLOT(onSegmentationProgressChanged(int)));
    this->connect(this, SIGNAL(evaluationProgressChanged(int)), this, SLOT(onEvaluationProgressChanged(int)));
}

void
mseg::MSegWidgetController::initComboAlgorithms(const std::vector<std::string> items)
{
    // Block all signals while re-initializing
    this->widget.comboAlgorithm->blockSignals(true);

    std::string oldText = this->widget.comboAlgorithm->currentText().toStdString();
    int oldIndex = this->widget.comboAlgorithm->currentIndex();
    int newIndex = 0; // Default is "Choose..."

    // Set combobox to initial state
    this->widget.comboAlgorithm->clear();

    if (items.size() == 0)
    {
        this->widget.comboAlgorithm->addItem("No Algorithms found", "Choose...");
    }
    else
    {
        this->widget.comboAlgorithm->addItem("Select Algorithm...", "Choose...");
    }

    // Populate combobox with given data
    for (std::string item : items)
    {
        ARMARX_INFO << "Registered '" << item << "' as available segmentation algorithm";
        this->widget.comboAlgorithm->addItem(QString::fromStdString(item), QString::fromStdString(item));
    }

    // Try to re-select the previous value (if it is still there, otherwise select "Choose...")
    if (oldIndex != 0)
    {
        newIndex = this->widget.comboAlgorithm->findData(QString::fromStdString(oldText));

        if (newIndex < 0)
        {
            emit algorithmSelected("");

            newIndex = 0;

            ARMARX_WARNING << "Connection to '" << oldText << "' lost";
        }
    }

    // Set the current index
    this->widget.comboAlgorithm->setCurrentIndex(newIndex);

    // Evaluate if the segment button is enabled or not
    this->updateButtonsSegment();

    // Re-enable signals
    this->widget.comboAlgorithm->blockSignals(false);
}

void
mseg::MSegWidgetController::initPollTimer()
{
    this->segmentPollTimer = new QTimer(this);
    this->connect(this->segmentPollTimer, SIGNAL(timeout()), this, SLOT(onSegmentPollTimerTimeout()));

    this->evaluationPollTimer = new QTimer(this);
    this->connect(this->evaluationPollTimer, SIGNAL(timeout()), this, SLOT(onEvaluationPollTimerTimeout()));
}

void
mseg::MSegWidgetController::reportSegmentationProgress(int percent, const Ice::Current&)
{
    emit segmentationProgressChanged(percent);
}

void
mseg::MSegWidgetController::reportEvaluationProgress(int percent, const Ice::Current&)
{
    emit evaluationProgressChanged(percent);
}

void
mseg::MSegWidgetController::updateButtonsSegment()
{
    this->widget.buttonSegmentCurrent->setEnabled(false);
    this->widget.buttonSegmentAll->setEnabled(false);

    if (this->widget.comboAlgorithm->currentIndex() != 0 && this->widget.widgetMotionSetPlayer->isMotionSetOpen())
    {
        this->widget.buttonSegmentAll->setEnabled(true);
        //this->widget.buttonSegmentCurrent->setEnabled(true);
    }
}

void
mseg::MSegWidgetController::updateButtonEvaluate()
{
    this->widget.buttonEvaluate->setEnabled(false);

    if (this->widget.comboAlgorithm->currentIndex() != 0 && this->segmentationResults.results.size() > 0 && this->widget.widgetMotionSetPlayer->isMotionSetOpen())
    {
        this->widget.buttonEvaluate->setEnabled(true);
    }
}

void
mseg::MSegWidgetController::onAlgorithmSelected(std::string algorithmName)
{
    // Clear widgets
    this->widget.widgetEvaluationResults->clearEvaluationResults();
    this->widget.widgetMotionSetPlayer->clearSegmentationResults();

    this->widget.progressSegmentation->setValue(0);
    this->widget.progressEvaluation->setValue(0);

    if (algorithmName != "")
    {
        try
        {
            // Get default algorithm parameters
            std::vector<mseg::AlgorithmParameter> algorithmParameters = this->segmentationController->getAlgorithmParameters(algorithmName);

            // Pass default parameters to parameter widget
            this->widget.widgetParameters->setSelectedAlgorithm(algorithmName, algorithmParameters);
        }
        catch (const mseg::AlgorithmOfflineException& e)
        {
            this->displayAlgorithmOfflineException();
        }
    }
    else
    {
        this->widget.widgetParameters->deselectAlgorithm();
    }

    // Update buttons
    this->updateButtonsSegment();
    this->updateButtonEvaluate();
}

void
mseg::MSegWidgetController::onButtonRefreshAlgorithmsClicked()
{
    std::vector<std::string> algorithms = this->segmentationController->querySegmentationAlgorithms();

    this->initComboAlgorithms(algorithms);
}

void
mseg::MSegWidgetController::onButtonSegmentCurrentClicked()
{
    ARMARX_WARNING << "NOT YET IMPLEMENTED";
}

void
mseg::MSegWidgetController::onButtonSegmentAllClicked()
{
    // Emit signal
    emit segmentationStarted();

    // Grey out buttons while computing
    this->widget.buttonSegmentCurrent->setEnabled(false);
    this->widget.buttonSegmentAll->setEnabled(false);
    this->widget.buttonEvaluate->setEnabled(false);
    this->widget.buttonRefreshAlgorithms->setEnabled(false);
    this->widget.comboAlgorithm->setEnabled(false);

    // Reset segmentation results
    this->segmentationResults.results.clear();

    // Reset evaluation results and progress
    this->widget.widgetEvaluationResults->clearEvaluationResults();
    this->widget.progressEvaluation->setValue(0);

    std::string algorithmName = this->widget.comboAlgorithm->currentText().toStdString();

    ARMARX_INFO << "Starting segmentation for '" << algorithmName << "'";

    try
    {
        // Collect set algorithm parameters and pass them to the segmentation controller
        std::vector<mseg::AlgorithmParameter> algorithmParameters = this->widget.widgetParameters->getParameterConfig();
        this->segmentationController->setAlgorithmParameters(algorithmName, algorithmParameters);

        // Start segmentation async
        this->segmentAsyncResult = this->segmentationController->begin_segmentDataset(algorithmName);

        // Make progress bar text visible
        this->widget.progressSegmentation->setTextVisible(true);

        // Set a timer to poll for segmentation completion
        this->segmentPollTimer->start(250);
    }
    catch (const mseg::AlgorithmOfflineException& e)
    {
        this->displayAlgorithmOfflineException();

        //this->widget.buttonSegmentCurrent->setEnabled(true);
        this->widget.buttonSegmentAll->setEnabled(true);
    }
    catch (const Ice::NotRegisteredException& e)
    {
        std::string title = "MSeg core module offline";
        std::string genericMessage = "The MSeg core module is needed in order to perform a segmentation.";
        std::string specificMessage = "Verify that the MSeg core module is online by running `mseg status` in the command line. \n"
                                      "If `stopped` or `mixed` is indicated, start it by running `msegcm start`.";

        this->displayException(title, genericMessage, specificMessage);

        //this->widget.buttonSegmentCurrent->setEnabled(true);
        this->widget.buttonSegmentAll->setEnabled(true);
    }
}

void
mseg::MSegWidgetController::onButtonEvaluateClicked()
{
    if (this->segmentationResults.results.size() == 0)
    {
        return;
    }

    // Emit signal
    emit evaluationStarted();

    // Grey out buttons while computing
    this->widget.buttonSegmentCurrent->setEnabled(false);
    this->widget.buttonSegmentAll->setEnabled(false);
    this->widget.buttonEvaluate->setEnabled(false);
    this->widget.buttonRefreshAlgorithms->setEnabled(false);
    this->widget.comboAlgorithm->setEnabled(false);

    this->evaluationAsyncResult = this->evaluationController->begin_evaluateAll(this->segmentationResults);

    // Make progress bar text visible
    this->widget.progressEvaluation->setTextVisible(true);

    this->evaluationPollTimer->start(250);
}

void
mseg::MSegWidgetController::onComboAlgorithmChanged()
{
    if (this->widget.comboAlgorithm->currentIndex() > 0)
    {
        // Get new algorithm name
        std::string algorithmName = this->widget.comboAlgorithm->currentText().toStdString();

        emit algorithmSelected(algorithmName);
    }
    else
    {
        emit algorithmSelected("");
    }
}

void
mseg::MSegWidgetController::onSegmentationProgressChanged(int percent)
{
    this->widget.progressSegmentation->setValue(percent);
}

void
mseg::MSegWidgetController::onEvaluationProgressChanged(int percent)
{
    this->widget.progressEvaluation->setValue(percent);
}

void
mseg::MSegWidgetController::onSegmentPollTimerTimeout()
{
    if (this->segmentAsyncResult->isCompleted())
    {
        bool success = false;

        this->segmentPollTimer->stop();

        // Manually override the progress in case messages got lost
        this->widget.progressSegmentation->setValue(100);

        try
        {
            // Fetch segmentation results from controller
            this->segmentationResults = this->segmentationController->end_segmentDataset(this->segmentAsyncResult);

            // Update segmentation result widget
            this->widget.widgetMotionSetPlayer->setSegmentationResults(this->segmentationResults.results);

            success = true;
        }
        catch (const mseg::AlgorithmOfflineException& e)
        {
            this->displayAlgorithmOfflineException();
        }
        catch (const mseg::AlgorithmException& e)
        {
            std::string title = "Error while segmenting";
            std::string genericMessage = "The segmentation algorithm faced an error while segmenting.";

            ARMARX_ERROR << genericMessage << "\n" << e.reason;
            this->displayException(title, genericMessage, e.reason);
        }
        catch (const mseg::DataHandlingException& e)
        {
            std::string title = "Error while handling with data";
            std::string genericMessage = "The segmentation algorithm faced an error while handling with the data exchange service.";

            ARMARX_ERROR << genericMessage << "\n" << e.reason;
            this->displayException(title, genericMessage, e.reason);
        }
        catch (const std::exception& e)
        {
            std::string title = "Unknown error";
            std::string genericMessage = "Unknown exception occured while segmenting.";

            ARMARX_ERROR << genericMessage << "\n" << e.what();
            this->displayException(title, genericMessage, e.what());
        }

        // Hide progress bar text
        this->widget.progressSegmentation->setTextVisible(false);

        // Update evaluate button
        this->updateButtonEvaluate();

        bool autoEvaluate = this->widget.checkAutoEvaluate->isChecked();

        // Emit signal
        emit segmentationFinished(success, autoEvaluate);

        // If auto-evaluate was checked, run evaluation now
        if (success && autoEvaluate)
        {
            this->onButtonEvaluateClicked();
        }
        else
        {
            // Enable segment button again if other conditions are met
            this->updateButtonsSegment();

            // Update algorithm selection
            this->widget.buttonRefreshAlgorithms->setEnabled(true);
            this->widget.comboAlgorithm->setEnabled(true);
        }
    }
}

void
mseg::MSegWidgetController::onEvaluationPollTimerTimeout()
{
    if (this->evaluationAsyncResult->isCompleted())
    {
        bool success = false;

        this->evaluationPollTimer->stop();

        // Manually override the progress in case messages got lost
        this->widget.progressEvaluation->setValue(100);

        try
        {
            // Fetch evaluation results from controller
            mseg::FormattedEvaluationResultPtr evaResults = this->evaluationController->end_evaluateAll(this->evaluationAsyncResult);

            // Hide progress bar text
            this->widget.progressEvaluation->setTextVisible(false);

            this->widget.widgetEvaluationResults->updateEvaluationResults(evaResults);

            success = true;
        }
        catch (...)
        {
        }

        // Enable segment button again if other conditions are met
        this->updateButtonsSegment();

        // Update evaluate button
        this->updateButtonEvaluate();

        // Update algorithm selection
        this->widget.buttonRefreshAlgorithms->setEnabled(true);
        this->widget.comboAlgorithm->setEnabled(true);

        // Show evaluations tab
        this->widget.widgetTabs->setCurrentWidget(this->widget.tabEvaluationResults);

        // Emit signal
        emit evaluationFinished(success);
    }
}

void mseg::MSegWidgetController::onDatasetLoading()
{
    this->widget.buttonSegmentCurrent->setEnabled(false);
    this->widget.buttonSegmentAll->setEnabled(false);
    this->widget.buttonEvaluate->setEnabled(false);
}

void mseg::MSegWidgetController::onDatasetChanged(mseg::MotionRecordingDataset dataset)
{
    // Set progress bars to 0
    this->widget.progressSegmentation->setValue(0);
    this->widget.progressEvaluation->setValue(0);

    this->widget.buttonSegmentCurrent->setEnabled(true);
    this->widget.buttonSegmentAll->setEnabled(true);
    this->widget.buttonEvaluate->setEnabled(false);

    this->widget.widgetEvaluationResults->clearEvaluationResults();
    this->segmentationController->setDataset(dataset);
    this->updateButtonsSegment();
}

void
mseg::MSegWidgetController::displayAlgorithmOfflineException()
{
    std::string title = "Algorithm offline";
    std::string genericMessage = "The selected motion segmentation algorithm seems to be offline.";
    std::string specificMessage = "Verify that the selected motion segmentation algorithm is running and try again.";

    this->displayException(title, genericMessage, specificMessage);
}

void
mseg::MSegWidgetController::displayException(std::string title, std::string genericMessage, std::string specificMessage)
{
    // Show a message box at the user interface, hinting the user to start those services
    // in the scenario manager
    QMessageBox msgBox;

    msgBox.setWindowTitle(QString::fromStdString(title));
    msgBox.setText(QString::fromStdString(genericMessage));
    msgBox.setInformativeText(QString::fromStdString(specificMessage));
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.exec();
}
