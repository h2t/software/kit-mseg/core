/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::gui-plugins::AlgorithmParameterWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_ArmarXCore_MSeg_AlgorithmParameterWidget_H
#define _ARMARX_ArmarXCore_MSeg_AlgorithmParameterWidget_H

#include <cmath> // pow
#include <string>

#include <boost/algorithm/string.hpp>

#include <ArmarXCore/core/logging/Logging.h>

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLocale>
#include <QPlainTextEdit>
#include <QString>
#include <QWidget>

#include <MSeg/interface/DataTypes.h>

namespace mseg
{
    class AlgorithmParameterWidget
        : public QWidget
    {

        Q_OBJECT

    protected:

        QWidget* widget;
        mseg::AlgorithmParameter algorithmParameter;

    public:

        AlgorithmParameterWidget(mseg::AlgorithmParameter);

        virtual ~AlgorithmParameterWidget();

        mseg::AlgorithmParameter getAlgorithmParameter();

    protected:

        virtual void initSubWidget();

        virtual QWidget* addBoolParameterWidget();
        virtual QWidget* addIntParameterWidget();
        virtual QWidget* addFloatParameterWidget();
        virtual QWidget* addStringParameterWidget();
        virtual QWidget* addJsonParameterWidget();

        double stringToFloat(std::string s);
        std::string floatToString(double number);
    };
}

#endif
