/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::GroundTruthIndicatorWidget
 * @author     Christian R. G. Dreher ( cdreher3011 at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_SEGMENTATIONX_GROUNDTRUTHINDICATORWIDGET_H
#define _ARMARX_SEGMENTATIONX_GROUNDTRUTHINDICATORWIDGET_H

#include <iostream>
#include <string>
#include <tuple>
#include <vector>

#include <QColor>
#include <QPainter>
#include <QPainterPath>
#include <QPaintEvent>
#include <QPen>
#include <QWidget>

namespace mseg
{

    class GroundTruthIndicatorWidget : public QWidget
    {
        Q_OBJECT

    private:

        int maxFrame;
        std::vector<std::tuple<int, int> > keyFrames;

    public:
        GroundTruthIndicatorWidget(QWidget* parent);
        virtual ~GroundTruthIndicatorWidget();

        void setMaxFrame(int);
        void setKeyFrames(std::vector<std::tuple<int, int> >);

    protected:

        void paintEvent(QPaintEvent* event);

    };

}

#endif
