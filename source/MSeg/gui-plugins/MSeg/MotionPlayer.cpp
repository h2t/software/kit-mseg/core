/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::MotionPlayer
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/MotionPlayer.h>

mseg::MotionPlayer::MotionPlayer(QWidget* container):
    viewer(new Viewer(container)),
    playing(false)
{
    this->_pSensorMgr = SoDB::getSensorManager();
    this->_pSensorTimer = new SoTimerSensor(timerCB, this);
    this->_pSensorTimer->setInterval(SbTime(this->ms_per_frame / 1000.0f));
}

mseg::MotionPlayer::~MotionPlayer()
{
    delete this->viewer;
}

void
mseg::MotionPlayer::loadC3D(std::string C3DFile)
{
    this->viewMode = MotionType::C3D;

    if (!C3DFile.empty())
    {
        MMM::LegacyMotionReaderC3DPtr c(new MMM::LegacyMotionReaderC3D());
        this->currentMarkerMotion = c->loadC3D(C3DFile);
        this->currentAbstractMotion = this->currentMarkerMotion;

        this->viewer->setupMarkerView(this->currentMarkerMotion);
        this->jumpToFrame(0);
        emit playProgress(0);
        emit fileChanged();
    }
}

void
mseg::MotionPlayer::loadMMM(std::string mmmFile)
{
    ARMARX_INFO << "Loading MMM data...";

    this->viewMode = MotionType::MMM;

    this->currentRobots = new std::map<std::string, VirtualRobot::RobotPtr>;
    this->currentMotions = new std::vector<MMM::LegacyMotionPtr>;
    this->currentNodeSets = new std::map<std::string, VirtualRobot::RobotNodeSetPtr>;
    this->currentFrame = 0;

    this->viewer->reset();

    MMM::LegacyMotionReaderXMLPtr r(new MMM::LegacyMotionReaderXML());

    std::string fileContent = mseg::Utility::readFile(mmmFile);
    std::string modelPath = mseg::Utility::paths::getMMMDataPath() + "/";
    std::vector<std::string> motionNames = r->getMotionNamesFromXMLString(fileContent);

    ARMARX_INFO << "Using MMM data path: " << modelPath;

    if (motionNames.size() == 0)
    {
        ARMARX_ERROR << "no motions in file ";
        return;
    }

    int frameCount = -1;

    for (size_t i = 0; i < motionNames.size(); i++)
    {
        MMM::LegacyMotionPtr motion = r->createMotionFromString(fileContent, motionNames[i], modelPath);

        if (!motion)
        {
            continue;
        }

        ARMARX_INFO << "Loading motion: " << motion->getName();

        if (frameCount != -1 && frameCount != (int) motion->getNumFrames())
        {
            ARMARX_ERROR << "Error whole loading motions: the frame count between two motions do not match. All motions need to have the same framecount.";
            return;
        }
        else
        {
            frameCount = motion->getNumFrames();
        }

        ARMARX_INFO  << "Model file: " << motion->getModel()->getFilename();

        if (motion->getModel())
        {
            (*this->currentRobots)[motion->getName()] = MMM::SimoxTools::buildModel(motion->getModel());
            MMM::SimoxTools::updateInertialMatricesFromModels((*this->currentRobots)[motion->getName()]);
        }

        VirtualRobot::RobotPtr robot = (*this->currentRobots)[motion->getName()];

        if (motion)
        {
            std::vector<std::string> jointNames = motion->getJointNames();

            if (jointNames.size() > 0)
            {
                std::string rnsName("MotionPlayerRNS");

                if (robot->hasRobotNodeSet(rnsName))
                {
                    robot->deregisterRobotNodeSet(robot->getRobotNodeSet(rnsName));
                }

                (*this->currentNodeSets)[motion->getName()] = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, rnsName, jointNames, "", "", true);
            }
        }

        this->currentMotions->push_back(motion);
    }

    this->currentAbstractMotion = boost::dynamic_pointer_cast<MMM::AbstractMotion>((*this->currentMotions)[0]);

    ARMARX_INFO << "Loading MMM data... done...";

    this->viewer->setupMMMView(*this->currentRobots, *this->currentMotions);
    this->jumpToFrame(0);
    emit playProgress(0);
    emit fileChanged();
}

void
mseg::MotionPlayer::progress()
{
    if (this->currentAbstractMotion && this->currentFrame + 1 < (int) this->currentAbstractMotion->getNumFrames())
    {
        this->jumpToFrame(++this->currentFrame);
    }
}

void
mseg::MotionPlayer::jumpToFrame(int frame)
{
    this->currentFrame = frame;

    switch (this->viewMode)
    {
        case MotionType::C3D:
            this->viewer->showMarkerFrame(this->currentMarkerMotion, frame);
            break;
        case MotionType::MMM:
            this->viewer->showMMMFrame(*this->currentRobots, *this->currentMotions, *this->currentNodeSets, frame);
            break;
    }

    emit playProgress(frame);
}

void
mseg::MotionPlayer::fpsChanged(double newValue)
{
    if (newValue < 0.1)
    {
        return;
    }

    double interval = 1. / newValue;
    this->_pSensorTimer->setInterval(SbTime(interval));
}

MMM::AbstractMotionPtr
mseg::MotionPlayer::getCurrentMotion()
{
    return this->currentAbstractMotion;
}

void
mseg::MotionPlayer::togglePlay()
{
    if (this->playing)
    {
        pause();
    }
    else
    {
        play();
    }
}

void
mseg::MotionPlayer::play()
{
    if (!this->playing)
    {
        this->_pSensorMgr->insertTimerSensor(this->_pSensorTimer);
    }

    this->playing = true;
}

void
mseg::MotionPlayer::pause()
{
    if (this->playing)
    {
        this->_pSensorMgr->removeTimerSensor(this->_pSensorTimer);
    }

    this->playing = false;
}

void
mseg::MotionPlayer::timerCB(void* data, SoSensor* sensor)
{
    MotionPlayer* self = static_cast<MotionPlayer*>(data);
    self->progress();
}

int
mseg::MotionPlayer::main()
{
    QtConcurrent::run(&SoQt::mainLoop);
    return 0;
}
