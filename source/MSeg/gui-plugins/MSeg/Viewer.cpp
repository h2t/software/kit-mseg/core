/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::gui-plugins::Viewer
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <MSeg/gui-plugins/MSeg/Viewer.h>

mseg::Viewer::Viewer(QWidget* container):
    QWidget(container)
{
    container->layout()->addWidget(this);

    this->sceneSep = new SoSeparator;
    this->sceneSep->ref();
    SoUnits* u = new SoUnits();
    u->units = SoUnits::MILLIMETERS;
    this->sceneSep->addChild(u);

    this->motionSep = new SoSeparator;
    this->sceneSep->addChild(motionSep);
    this->rootCoordSep = new SoSeparator;
    this->sceneSep->addChild(rootCoordSep);
    this->markerSep = new SoSeparator;
    this->sceneSep->addChild(markerSep);

    this->floorSep = new SoSeparator;
    this->swFloor = new SoSwitch;

    // TODO: re-insert floor
    this->sceneSep->addChild(swFloor);
    this->swFloor->addChild(floorSep);

    // sceneSep->addChild(floorSep); ohne SoSwitch
    this->swFloor->whichChild = SO_SWITCH_ALL;
    //swFloor->whichChild=SO_SWITCH_NONE;

    Eigen::Vector3f up;
    up << 0.0f, 0.0f, 1.0f;
    Eigen::Vector3f pos;
    pos << 0, 0, 0;
    SoSeparator* f = VirtualRobot::CoinVisualizationFactory::CreatePlaneVisualization(pos, up, 10000.0f, 0);
    this->floorSep->addChild(f);

    this->canvas = new SoQtExaminerViewer(this);

    // setup
    this->canvas->setBackgroundColor(SbColor(1.0f, 1.0f, 1.0f));
    this->canvas->setAccumulationBuffer(true);

    this->canvas->setGLRenderAction(new SoLineHighlightRenderAction);
    this->canvas->setTransparencyType(SoGLRenderAction::BLEND);
    this->canvas->setFeedbackVisibility(true);
    this->canvas->setDecoration(false);
    this->canvas->setSceneGraph(sceneSep);
    //canvas->setEventCallback(&MMMViewerWindow::eventCallback, this);
    this->canvas->viewAll();
}

mseg::Viewer::~Viewer()
{
    this->sceneSep->unref();
    delete this->canvas;
}

void
mseg::Viewer::reset()
{
    this->markerSep->removeAllChildren();
    this->swFloor->whichChild = SO_SWITCH_ALL;

    // remove robot visu
    for (std::map<std::string, SoSeparator*>::iterator it = this->robotSeps.begin(); it != this->robotSeps.end(); it++)
    {
        if (it->second)
        {
            it->second->removeAllChildren();
        }
    }

    if (rootCoordSep->getNumChildren() == 0)
    {
        std::string rootTXT("Root Coordinate System");
        this->rootCoordSep->addChild(VirtualRobot::CoinVisualizationFactory::CreateCoordSystemVisualization(1.0f, &rootTXT));
    }
}

void
mseg::Viewer::setupMMMView(std::map<std::string, VirtualRobot::RobotPtr>& robots, std::vector<MMM::LegacyMotionPtr>& motions)
{
    this->reset();

    for (MMM::LegacyMotionList::iterator it = motions.begin(); it != motions.end(); it++)
    {
        MMM::LegacyMotionPtr motion = *it;
        VirtualRobot::RobotPtr robot = robots[motion->getName()];
        std::map<std::string, SoSeparator*>::iterator itSep = this->robotSeps.find(motion->getName());
        
        if (itSep == robotSeps.end())
        {
            itSep = robotSeps.insert(std::make_pair(motion->getName(), new SoSeparator())).first;
            this->sceneSep->addChild(itSep->second);
        }

        SoSeparator* robotSep = itSep->second;

        if (robot && robotSep->getNumChildren() == 0)
        {
            bool useColModel = false;
            VirtualRobot::SceneObject::VisualizationType colModelType = (useColModel) ? VirtualRobot::SceneObject::Collision : VirtualRobot::SceneObject::Full;

            boost::shared_ptr<VirtualRobot::CoinVisualization> visualization = robot->getVisualization<VirtualRobot::CoinVisualization>(colModelType);
            SoNode* visualisationNode = NULL;
            
            if (visualization)
            {
                visualisationNode = visualization->getCoinVisualization();
            }

            if (visualisationNode)
            {
                robotSep->addChild(visualisationNode);
            }
        }
    }
}

void
mseg::Viewer::setupMarkerView(MMM::MarkerMotionPtr& markerMotion)
{
    this->reset();

    if (!markerMotion)
    {
        return;
    }

    MMM::LegacyMarkerDataPtr markerData = markerMotion->getFrame(0);
    
    if (!markerData)
    {
        return;
    }
    
    std::map<std::string, Eigen::Vector3f> data = markerData->data;
    SoSphere* sphere = new SoSphere;
    sphere->radius = 10;

    for (std::map<std::string, Eigen::Vector3f>::iterator it = data.begin(); it != data.end(); it++)
    {
        //create a sphere for each marker
        SoSeparator* sphereSep = new SoSeparator;
        Eigen::Matrix4f globalPose = Eigen::Matrix4f::Identity();
        globalPose.block(0, 3, 3, 1) = it->second;
        SoMaterial* marked = new SoMaterial;
        marked->ambientColor.setValue(0.2f, 0.2f, 1.0f);
        marked->diffuseColor.setValue(0.2f, 0.2f, 1.0f);
        marked->specularColor.setValue(0.5f, 0.5f, 0.5f);
        sphereSep->addChild(marked);

        sphereSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(globalPose));
        sphereSep->addChild(sphere);

        //create marker labels
        SoSeparator* labelSep = new SoSeparator;
        Eigen::Vector3f labelVector;
        labelVector << 0, 30, 0;
        Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();
        localPose.block(0, 3, 3, 1) += labelVector;
        localPose.block(0, 0, 3, 3) *= 2;
        //labelSep->addChild(marked);

        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::getMatrixTransform(localPose));
        std::string txt = "marker";
        
        if (!it->first.empty())
        {
            txt = it->first;
        }

        labelSep->addChild(VirtualRobot::CoinVisualizationFactory::CreateBillboardText(txt));

        sphereSep->addChild(labelSep);
        this->markerSep->addChild(sphereSep);
    }
}

void
mseg::Viewer::showMMMFrame(std::map<std::string, VirtualRobot::RobotPtr>& robots, std::vector<MMM::LegacyMotionPtr>& motions, std::map<std::string, VirtualRobot::RobotNodeSetPtr>& nodeSets, int frame)
{
    for (MMM::LegacyMotionList::iterator it = motions.begin(); it != motions.end(); it++)
    {
        MMM::LegacyMotionPtr motion = *it;
        VirtualRobot::RobotPtr robot = robots[motion->getName()];
        VirtualRobot::RobotNodeSetPtr rns = nodeSets[motion->getName()];

        if (!motion || frame < 0 || frame >= (int)motion->getNumFrames() || !robot)
        {
            continue;
        }

        // update model/robot

        MMM::MotionFramePtr md = motion->getMotionFrame(frame);
        
        if (!md)
        {
            std::cout << "internal error" << std::endl;
            continue;
        }
        
        robot->setGlobalPose(md->getRootPose());
        
        if (rns)
        {
            if (md->joint.rows() != rns->getSize())
            {
                MMM_ERROR << "converted joint data does not fit size of model's kinematic chain:" << md->joint.rows() << "!=" << rns->getSize() << std::endl;
                continue;
            }
            
            rns->setJointValues(md->joint);
        }
    }
}

void
mseg::Viewer::showMarkerFrame(MMM::MarkerMotionPtr& markerMotion, int frame)
{
    if (this->markerSep->getNumChildren() == 0)
    {
        setupMarkerView(markerMotion);
    }

    if (!markerMotion)
    {
        return;
    }

    MMM::LegacyMarkerDataPtr markerData = markerMotion->getFrame(frame);
    
    if (!markerData)
    {
        return;
    }
    
    std::map<std::string, Eigen::Vector3f> data = markerData->data;
    
    if ((int)this->markerSep->getNumChildren() < (int)data.size())
    {
        std::cout << "internal error..." << std::endl;
        return;
    }

    size_t pos = 0;
    
    for (std::map<std::string, Eigen::Vector3f>::iterator it = data.begin(); it != data.end(); it++)
    {
        SoSeparator* s = (SoSeparator*)this->markerSep->getChild(pos);
        
        if (s->getNumChildren() < 3)
        {
            std::cout << "int err" << std::endl;
            return;
        }

        // set pose
        Eigen::Matrix4f globalPose = Eigen::Matrix4f::Identity();
        globalPose.block(0, 3, 3, 1) = it->second;
        SoMatrixTransform* mt = (SoMatrixTransform*)s->getChild(1);
        SbMatrix m_(reinterpret_cast<SbMat*>(globalPose.data()));
        mt->matrix.setValue(m_);
        pos++;
    }
}
